// 输入缓冲区的相关实现
use alloc::string::String;
use crossbeam_queue::ArrayQueue;
use lazy_static::lazy_static;
use crate::drivers::serial::{get_serial, get_serial_for_sure};
use alloc::vec::Vec;

// 定义输入类型，这里我们简单地使用u8，代表ASCII字符
type Key = u8;

// 使用lazy_static初始化一个全局的输入缓冲区
lazy_static! {
    pub static ref INPUT_BUF: ArrayQueue<Key> = ArrayQueue::new(128);
}

// 向缓冲区中推送一个键值，如果缓冲区满了则打印警告
#[inline]
pub fn push_key(key: Key) {
    if INPUT_BUF.push(key).is_err() {
        println!("Input buffer is full. Dropping key '{:?}'", key);
    }
}

// 尝试从缓冲区中弹出一个键值
#[inline]
pub fn try_pop_key() -> Option<Key> {
    INPUT_BUF.pop()
}

// 从缓冲区中阻塞取出数据，循环等待直到缓冲区中有数据可取
pub fn pop_key() -> Key {
    loop {
        if let Some(key) = try_pop_key() {
            return key;
        }
        x86_64::instructions::hlt();
    }
}

// 检查字节序列是否构成了一个完整的UTF-8字符
fn is_complete_utf8_char(bytes: &[u8]) -> bool {
    match bytes.len() {
        1 => bytes[0] & 0x80 == 0, // 对于1字节的ASCII字符
        2 => bytes[0] & 0xE0 == 0xC0, // 对于2字节的字符
        3 => bytes[0] & 0xF0 == 0xE0, // 对于3字节的字符
        4 => bytes[0] & 0xF8 == 0xF0, // 对于4字节的字符
        _ => false,
    }
}

pub fn get_line() -> String {
    let mut line = String::with_capacity(128);
    let mut utf8_bytes = Vec::new();
    loop {
        let key = pop_key(); // 阻塞等待，直到缓冲区中有数据
        utf8_bytes.push(key);

        // 检查是否收集到了完整的UTF-8字符
        if is_complete_utf8_char(&utf8_bytes) {
            if let Ok(s) = core::str::from_utf8(&utf8_bytes) {
                match s {
                    "\n" | "\r" => {
                        println!();
                        break;
                    },
                    "\x08" | "\x7F" => { // 处理退格键
                        if !line.is_empty() {
                            let last_utf8_len = line.pop().unwrap().len_utf8();
                            let utf8_display_width = match last_utf8_len {
                                1 => 1,
                                2 => 2,
                                3 => 2,
                                4 => 2,
                                _ => 0,
                            };
                            for _ in 0..utf8_display_width {
                                get_serial_for_sure().backspace(); // 从屏幕上删除一个字符
                            }
                        }
                    },
                    _ => {
                        line.push_str(s);
                        print!("{}", s);
                    },
                }
            }
            utf8_bytes.clear(); // 清空缓冲区以接收下一个字符
        }
    }
    line
}
