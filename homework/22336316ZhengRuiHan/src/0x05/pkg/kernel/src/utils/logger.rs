use log::{Metadata, Record, Level, LevelFilter};

pub fn init(log_level: &str) {
    static LOGGER: Logger = Logger;
    log::set_logger(&LOGGER).unwrap();
    let level_filter = match log_level {
        _ if log_level.eq_ignore_ascii_case("off") => LevelFilter::Off,
        _ if log_level.eq_ignore_ascii_case("error") => LevelFilter::Error,
        _ if log_level.eq_ignore_ascii_case("warn") => LevelFilter::Warn,
        _ if log_level.eq_ignore_ascii_case("info") => LevelFilter::Info,
        _ if log_level.eq_ignore_ascii_case("debug") => LevelFilter::Debug,
        _ if log_level.eq_ignore_ascii_case("trace") => LevelFilter::Trace,
        _ => LevelFilter::Info, // Default level or handle unknown level as you see fit
    };
    log::set_max_level(level_filter);
    info!("Logger Initialized.");
}

struct Logger;

impl log::Log for Logger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("[{}] {}", record.level(), record.args());
        }
    }

    fn flush(&self) {}
}
