#[macro_use]
mod macros;
#[macro_use]
mod regs;

// pub mod clock;
pub mod func;
pub mod logger;
pub use macros::*;
pub use regs::*;
pub mod resource;
use crate::format;

use crate::proc::*;
use crate::utils;
pub const fn get_ascii_header() -> &'static str {
    concat!(
        r"
__  __      __  _____            ____  _____
\ \/ /___ _/ /_/ ___/___  ____  / __ \/ ___/
 \  / __ `/ __/\__ \/ _ \/ __ \/ / / /\__ \
 / / /_/ / /_ ___/ /  __/ / / / /_/ /___/ /
/_/\__,_/\__//____/\___/_/ /_/\____//____/

                                       v",
        env!("CARGO_PKG_VERSION")
    )
}

// pub fn new_test_thread(id: &str) -> ProcessId {
//     let mut proc_data = ProcessData::new();
//     proc_data.set_env("id", id);

//     // spawn_kernel_thread(
//     //     utils::func::test,
//     //     format!("#{}_test", id),
//     //     Some(proc_data),
//     // )
// }

// pub fn new_stack_test_thread() {
//     let pid = spawn_kernel_thread(
//         utils::func::stack_test,
//         alloc::string::String::from("stack"),
//         None,
//     );

//     // wait for progress exit
//     wait(pid);
// }

fn wait(pid: ProcessId) {
    loop {
        // try to get the status of the process
        // HINT: it's better to use the exit code
        let proc_manager = get_process_manager();

        if let Some(exit_code) = proc_manager.get_exit_code(pid) {
            info!("Process {} exited with code {}", pid, exit_code);
            break; // 退出循环
        } else {
            // 让出CPU时间片，避免紧密循环导致的CPU资源浪费
            x86_64::instructions::hlt();
        }
    }
}
