use crate::{memory::gdt, proc::*};
use alloc::format;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};
use crate::proc::ProgramStatus;

// NOTE: import `ysos_syscall` package as `syscall_def` in Cargo.toml
use syscall_def::Syscall;
use crate::interrupt::consts::*;
mod service;
use super::consts;
use crate::memory::gdt::SYSCALL_IST_INDEX;

// FIXME: write syscall service handler in `service.rs`
use service::*;

pub unsafe fn register_idt(idt: &mut InterruptDescriptorTable) {
    // register syscall handler to IDT
    //        - standalone syscall stack
    //        - ring 3
    idt[Interrupts::Syscall as u8]
        .set_handler_fn(syscall_handler)
        .set_stack_index(SYSCALL_IST_INDEX as u16) // 使用独立的栈
        .set_privilege_level(x86_64::PrivilegeLevel::Ring3); // 设置DPL为3
}

pub extern "C" fn syscall(mut context: ProcessContext) {
    // debug!("Syscall interrupt received. context: {:?}", context);
    x86_64::instructions::interrupts::without_interrupts(|| {
        super::syscall::dispatcher(&mut context);
    });
}

as_handler!(syscall);

#[derive(Clone, Debug)]
pub struct SyscallArgs {
    pub syscall: Syscall,
    pub arg0: usize,
    pub arg1: usize,
    pub arg2: usize,
}

pub fn dispatcher(context: &mut ProcessContext) {
    let args = super::syscall::SyscallArgs::new(
        Syscall::from(context.regs.rax),
        context.regs.rdi,
        context.regs.rsi,
        context.regs.rdx,
    );

    // NOTE: you may want to trace syscall arguments
    // trace!("syscall received. args: {}", args);

    match args.syscall {
        // fd: arg0 as u8, buf: &[u8] (ptr: arg1 as *const u8, len: arg2)
        Syscall::Read => { 
            /* read from fd & return length */
            context.set_rax(sys_read(&args));
        },
        // fd: arg0 as u8, buf: &[u8] (ptr: arg1 as *const u8, len: arg2)
        Syscall::Write => { 
            /* write to fd & return length */
            context.set_rax(sys_write(&args));
        },
        // None -> pid: u16
        Syscall::GetPid => { 
            /* get current pid */ 
            context.set_rax(get_process_manager().current().pid().0.into());
        },

        // None -> pid: u16 or 0 or -1
        Syscall::Fork => {
            // 由于父子进程需要不同返回值, rax在内部设置. 
            fork_process(&args, context);
        }, 

        // path: &str (ptr: arg0 as *const u8, len: arg1) -> pid: u16
        Syscall::Spawn => { /* spawn process from name */
            context.set_rax(spawn_process(&args));
        },
        // ret: arg0 as isize
        Syscall::Exit => { /* exit process with retcode */
            exit_process(&args, context);
            context.set_rax(args.arg0);
        },
        // pid: arg0 as u16 -> status: isize
        Syscall::WaitPid => {
            /* wait for process with pid */
            // 被等待者不需要设置rax, 而等待者需要(当被等待者为dead, 设置为被等待者的返回值)
            // 所以, 在内部设置rax, 而不在这里
            sys_wait_pid(&args, context);
        },

        // None
        Syscall::Stat => { /* list processes */ 
            list_process();
        },
        // None
        Syscall::ListApp => { /* list available apps */
            list_app();
        },

        // Sleep
        Syscall::Time => {
            context.set_rax(sys_time(&args));
        },

        // op: u8, key: u32, val: usize -> ret: any
        Syscall::Sem => {
            sys_sem(&args, context)
        }

        // ----------------------------------------------------
        // NOTE: following syscall examples are implemented
        // ----------------------------------------------------

        // layout: arg0 as *const Layout -> ptr: *mut u8
        Syscall::Allocate => context.set_rax(sys_allocate(&args)),
        // ptr: arg0 as *mut u8
        Syscall::Deallocate => sys_deallocate(&args),
        // Unknown
        Syscall::Unknown => warn!("Unhandled syscall: {:x?}", context.regs.rax),
    }
}

impl SyscallArgs {
    pub fn new(syscall: Syscall, arg0: usize, arg1: usize, arg2: usize) -> Self {
        Self {
            syscall,
            arg0,
            arg1,
            arg2,
        }
    }
}

impl core::fmt::Display for SyscallArgs {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "SYSCALL: {:<10} (0x{:016x}, 0x{:016x}, 0x{:016x})",
            format!("{:?}", self.syscall),
            self.arg0,
            self.arg1,
            self.arg2
        )
    }
}
