// 引入memory模块，这个模块可能包含一些关于内存管理的定义或函数。
use crate::memory::*;
// 引入x86_64包中的Cr2寄存器相关的功能，Cr2寄存器在发生页故障时存储引发故障的地址。
use x86_64::registers::control::Cr2;
// 引入x86_64包中的IDT（中断描述表）相关的结构体和枚举。
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode};
use x86_64::VirtAddr;

// 定义一个函数用于注册中断处理程序到IDT。
pub unsafe fn register_idt(idt: &mut InterruptDescriptorTable) {
    idt.divide_error.set_handler_fn(divide_error_handler); // 0x00
    idt.debug.set_handler_fn(debug_handler); // 0x01
    idt.non_maskable_interrupt.set_handler_fn(non_maskable_interrupt_handler); // 0x02
    idt.breakpoint.set_handler_fn(breakpoint_handler); // 0x03
    idt.overflow.set_handler_fn(overflow_handler); // 0x04
    idt.bound_range_exceeded.set_handler_fn(bound_range_exceeded_handler); // 0x05
    idt.invalid_opcode.set_handler_fn(invalid_opcode_handler); // 0x06
    idt.device_not_available.set_handler_fn(device_not_available_handler); // 0x07
    idt.double_fault.set_handler_fn(double_fault_handler)
        .set_stack_index(gdt::DOUBLE_FAULT_IST_INDEX); // 0x08
    idt.invalid_tss.set_handler_fn(invalid_tss_handler); // 0x0A
    idt.segment_not_present.set_handler_fn(segment_not_present_handler); // 0x0B
    idt.stack_segment_fault.set_handler_fn(stack_segment_fault_handler); // 0x0C
    idt.general_protection_fault.set_handler_fn(general_protection_fault_handler)
        .set_stack_index(gdt::GENERAL_PROTECTION_FAULT_IST_INDEX); // 0x0D
    idt.page_fault.set_handler_fn(page_fault_handler)
        .set_stack_index(gdt::PAGE_FAULT_IST_INDEX); // 0x0E
    idt.x87_floating_point.set_handler_fn(x87_floating_point_handler); // 0x10
    idt.alignment_check.set_handler_fn(alignment_check_handler); // 0x11
    idt.machine_check.set_handler_fn(machine_check_handler); // 0x12
    idt.simd_floating_point.set_handler_fn(simd_floating_point_handler); // 0x13
    idt.virtualization.set_handler_fn(virtualization_handler); // 0x14
    idt.cp_protection_exception.set_handler_fn(cp_protection_exception_handler); // 0x15
    idt.hv_injection_exception.set_handler_fn(hv_injection_exception_handler); // 0x1C
    idt.vmm_communication_exception.set_handler_fn(vmm_communication_exception_handler); // 0x1D
    idt.security_exception.set_handler_fn(security_exception_handler); // 0x1E
}


pub extern "x86-interrupt" fn divide_error_handler(stack_frame: InterruptStackFrame) { // 0x00
    panic!("EXCEPTION: DIVIDE ERROR\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn debug_handler(stack_frame: InterruptStackFrame) { // 0x01
    panic!("EXCEPTION: DEBUG\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn non_maskable_interrupt_handler(stack_frame: InterruptStackFrame) { // 0x02
    panic!("EXCEPTION: NON-MASKABLE INTERRUPT\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn breakpoint_handler(stack_frame: InterruptStackFrame) { // 0x03
    panic!("EXCEPTION: BREAKPOINT\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn overflow_handler(stack_frame: InterruptStackFrame) { // 0x04
    panic!("EXCEPTION: OVERFLOW\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn bound_range_exceeded_handler(stack_frame: InterruptStackFrame) { // 0x05
    panic!("EXCEPTION: BOUND RANGE EXCEEDED\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn invalid_opcode_handler(stack_frame: InterruptStackFrame) { // 0x06
    panic!("EXCEPTION: INVALID OPCODE\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn device_not_available_handler(stack_frame: InterruptStackFrame) { // 0x07
    panic!("EXCEPTION: DEVICE NOT AVAILABLE\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn double_fault_handler( // 0x08
    stack_frame: InterruptStackFrame,
    error_code: u64,
) -> ! {
    panic!(
        "EXCEPTION: DOUBLE FAULT, ERROR_CODE: 0x{:016x}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn invalid_tss_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x0A
    panic!(
        "EXCEPTION: INVALID TSS, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn segment_not_present_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x0B
    panic!(
        "EXCEPTION: SEGMENT NOT PRESENT, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn stack_segment_fault_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x0C
    panic!(
        "EXCEPTION: STACK-SEGMENT FAULT, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn general_protection_fault_handler( // 0x0D
    stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    panic!(
        "EXCEPTION: GENERAL PROTECTION FAULT, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn page_fault_handler(
    stack_frame: InterruptStackFrame,
    err_code: PageFaultErrorCode,
) {
    if !crate::proc::handle_page_fault(Cr2::read().unwrap(), err_code) {
        error!(
            "EXCEPTION: PAGE FAULT, ERROR_CODE: {:?}\n\nPID {:?} Trying to access: {:#x}\n{:#?}",
            err_code,
            crate::proc::get_process_manager().current().pid(),
            Cr2::read().unwrap(),
            stack_frame
        );
        panic!("Cannot handle page fault!");
    }
}

pub extern "x86-interrupt" fn x87_floating_point_handler(stack_frame: InterruptStackFrame) { // 0x10
    panic!("EXCEPTION: x87 FLOATING POINT\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn alignment_check_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x11
    panic!(
        "EXCEPTION: ALIGNMENT CHECK, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn machine_check_handler(stack_frame: InterruptStackFrame) -> ! { // 0x12
    panic!("EXCEPTION: MACHINE CHECK\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn simd_floating_point_handler(stack_frame: InterruptStackFrame) { // 0x13
    panic!("EXCEPTION: SIMD FLOATING POINT\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn virtualization_handler(stack_frame: InterruptStackFrame) { // 0x14
    panic!("EXCEPTION: VIRTUALIZATION\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn cp_protection_exception_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x15
    panic!(
        "EXCEPTION: COPROCESSOR PROTECTION EXCEPTION, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn hv_injection_exception_handler(stack_frame: InterruptStackFrame) { // 0x1C
    panic!("EXCEPTION: HV INJECTION EXCEPTION\n\n{:#?}", stack_frame);
}

pub extern "x86-interrupt" fn vmm_communication_exception_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x1D
    panic!(
        "EXCEPTION: VMM COMMUNICATION EXCEPTION, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}

pub extern "x86-interrupt" fn security_exception_handler(stack_frame: InterruptStackFrame, error_code: u64) { // 0x1E
    panic!(
        "EXCEPTION: SECURITY EXCEPTION, ERROR CODE: {}\n\n{:#?}",
        error_code, stack_frame
    );
}
