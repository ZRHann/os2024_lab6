use super::LocalApic;
use bit_field::BitField;
use core::fmt::{Debug, Error, Formatter};
use core::ptr::{read_volatile, write_volatile};
use x86::cpuid::CpuId;
use crate::interrupt::consts::{Interrupts, Irq};

// 默认的xAPIC物理地址常量
pub const LAPIC_ADDR: u64 = 0xFEE00000;

// https://wiki.osdev.org/APIC
bitflags! {
    struct LapicRegister: u32 {
        const LAPIC_ID = 0x020; // LAPIC ID Register
        const LAPIC_VERSION = 0x030; // LAPIC Version Register
        const TPR = 0x080; // Task Priority Register
        const APR = 0x090; // Arbitration Priority Register
        const PPR = 0x0A0; // Processor Priority Register
        const EOI = 0x0B0; // EOI Register
        const RRD = 0x0C0; // Remote Read Register
        const LOGICAL_DEST = 0x0D0; // Logical Destination Register
        const DEST_FORMAT = 0x0E0; // Destination Format Register
        const SPIV = 0x0F0; // Spurious Interrupt Vector Register
        const ISR = 0x100; // In-Service Register (Start of ISR registers)
        const TMR = 0x180; // Trigger Mode Register (Start of TMR registers)
        const IRR = 0x200; // Interrupt Request Register (Start of IRR registers)
        const ERROR_STATUS = 0x280; // Error Status Register
        const LVT_CMCI = 0x2F0; // LVT Corrected Machine Check Interrupt Register
        const ICR_LOW = 0x300; // Interrupt Command Register Low
        const ICR_HIGH = 0x310; // Interrupt Command Register High
        const LVT_TIMER = 0x320; // LVT Timer Register
        const LVT_THERMAL_SENSOR = 0x330; // LVT Thermal Sensor Register
        const LVT_PERF_MONITORING = 0x340; // LVT Performance Monitoring Counters Register
        const LVT_LINT0 = 0x350; // LVT LINT0 Register
        const LVT_LINT1 = 0x360; // LVT LINT1 Register
        const LVT_ERROR = 0x370; // LVT Error Register
        const INITIAL_COUNT = 0x380; // Initial Count Register
        const CURRENT_COUNT = 0x390; // Current Count Register
        const DIVIDE_CONFIG = 0x3E0; // Divide Configuration Register
    }

    /// Defines flags for the Spurious Interrupt Vector Register (SPIV)
    pub struct SpivFlags: u32 {
        /// The interrupt vector for the spurious interrupt. Typically set to 0xFF.
        const VECTOR_MASK = 0xFF;
        /// Bit to enable the APIC. Set this to enable.
        const APIC_ENABLE = 0x100;
        /// If set, EOI messages are not broadcasted on handling spurious interrupts.
        const EOI_BROADCAST_SUPPRESSION = 0x1000;
    }

    pub struct LvtTimerFlags: u32 {
        const VECTOR_MASK = 0xFF;
        const MASK = 1 << 16;
        const PERIODIC = 1 << 17;
    }

    pub struct IcrLowFlags: u32 {
        const VECTOR_MASK = 0xFF;
        const DELIVERY_MODE_NORMAL = 0b000 << 8;
        const DELIVERY_MODE_LOWEST_PRIORITY = 0b001 << 8;
        const DELIVERY_MODE_SMI = 0b010 << 8;
        const DELIVERY_MODE_NMI = 0b100 << 8;
        const DELIVERY_MODE_INIT = 0b101 << 8;
        const DELIVERY_MODE_SIPI = 0b110 << 8;
        const DESTINATION_MODE_PHYSICAL = 0 << 11;
        const DESTINATION_MODE_LOGICAL = 1 << 11;
        const DELIVERY_STATUS = 1 << 12;
        // Bit 13 is reserved
        const INIT_LEVEL_DEASSERT = 0 << 14; // This and the next flag can be combined to represent bit 14 clear and bit 15 set respectively
        const LEVEL = 1 << 15;
        const DESTINATION_SHORTHAND_NONE = 0b00 << 18;
        const DESTINATION_SHORTHAND_SELF = 0b01 << 18;
        const DESTINATION_SHORTHAND_ALL_INCLUDING_SELF = 0b10 << 18;
        const DESTINATION_SHORTHAND_ALL_EXCLUDING_SELF = 0b11 << 18;
        // Bits 20-31 are reserved
    }

    pub struct IcrHighFlags: u32 {
        const DESTINATION_FIELD_MASK = 0x0F000000; // bits 24-27 are used for the APIC ID
    }
}


// 定义XApic结构体，代表一个xAPIC
pub struct XApic {
    addr: u64, // xAPIC的基地址
}

impl XApic {
    // 创建一个新的XApic实例，自动将物理地址转换为虚拟地址
    pub unsafe fn new(addr: u64) -> Self {
        XApic { addr }
    }

    // 从指定的寄存器读取一个值
    unsafe fn read(&self, reg: LapicRegister) -> u32 {
        read_volatile((self.addr + reg.bits() as u64) as *const u32)
    }

    // 向指定的寄存器写入一个值
    unsafe fn write(&mut self, reg: LapicRegister, value: u32) {
        write_volatile((self.addr + reg.bits() as u64) as *mut u32, value);
        self.read(LapicRegister::SPIV); // 读取一个寄存器, 确保写操作完成
    }
}

// 实现LocalApic trait，为XApic提供APIC相关的操作
impl LocalApic for XApic {
    // 检查当前系统是否支持xAPIC
    fn support() -> bool {
        let cpuid = CpuId::new();
        match cpuid.get_feature_info() {
            Some(feat_info) => feat_info.has_apic(),
            None => false,
        }
    }
    // 初始化当前CPU的xAPIC
    fn cpu_init(&mut self) {
        unsafe {
            // 启用本地APIC；设置spurious中断向量
            let mut spiv = SpivFlags::from_bits_truncate(self.read(LapicRegister::SPIV));
            spiv.insert(SpivFlags::APIC_ENABLE); // set EN bit
            spiv.remove(SpivFlags::VECTOR_MASK); // 清除低8位（中断向量）
            let irq_spurious_vector = Interrupts::IrqBase as u32 + Irq::Spurious as u32;
            let new_spiv_value = spiv.bits() | (irq_spurious_vector & SpivFlags::VECTOR_MASK.bits()); // 设置低8位（中断向量）
            self.write(LapicRegister::SPIV, new_spiv_value);

            // 设置定时器以按总线频率重复计数
            let mut lvt_timer_flags = LvtTimerFlags::from_bits_truncate(self.read(LapicRegister::LVT_TIMER));
            lvt_timer_flags.remove(LvtTimerFlags::VECTOR_MASK); // 清除现有的向量值
            let irq_timer_vector = Interrupts::IrqBase as u32 + Irq::Timer as u32;
            lvt_timer_flags |= LvtTimerFlags::from_bits_truncate(irq_timer_vector); // 设置新的定时器中断向量
            lvt_timer_flags.remove(LvtTimerFlags::MASK); // 清除Mask位，以启用定时器中断
            lvt_timer_flags.insert(LvtTimerFlags::PERIODIC); // 设置定时器为周期模式
            self.write(LapicRegister::LVT_TIMER, lvt_timer_flags.bits()); // 将修改后的值写回LVT Timer寄存器
            
            // 写入分频配置寄存器
            self.write(LapicRegister::DIVIDE_CONFIG, 0x0);
            info!("LapicRegister::INITIAL_COUNT: {:?}", self.read(LapicRegister::INITIAL_COUNT));
            self.write(LapicRegister::INITIAL_COUNT, 0x4FFFF);
            
            // 000: Divide by 2
            // 001: Divide by 4
            // 010: Divide by 8
            // 011: Divide by 16
            // 100: Divide by 32
            // 101: Divide by 64
            // 110: Divide by 128
            // 111: Divide by 1
            // 128 is faster than 1

            // 禁用逻辑中断线（LINT0，LINT1）
            self.write(LapicRegister::LVT_LINT0, 1 << 16); // Mask LINT0
            self.write(LapicRegister::LVT_LINT1, 1 << 16); // Mask LINT1

            // 禁用性能计数器溢出中断（PCINT）
            self.write(LapicRegister::LVT_PERF_MONITORING, self.read(LapicRegister::LVT_PERF_MONITORING) | (1 << 16)); // Mask PCINT

            // 将错误中断映射到IRQ_ERROR
            let irq_error = Interrupts::IrqBase as u32 + Irq::Error as u32; // 使用Irq::Error来获取错误中断向量号
            self.write(LapicRegister::LVT_ERROR, irq_error); // 写入LVT错误寄存器

            // 清除错误状态寄存器（需要连续写入两次）
            self.write(LapicRegister::ERROR_STATUS, 0);
            self.write(LapicRegister::ERROR_STATUS, 0);

            // 确认任何未处理的中断
            self.write(LapicRegister::EOI, 0); // 向EOI寄存器写入0以确认任何挂起的中断

            // 发送一个Init Level De-Assert来同步仲裁ID
            self.write(LapicRegister::ICR_HIGH, 0); // set ICR 0x310
            let icr_low_flags = IcrLowFlags::DELIVERY_MODE_INIT | IcrLowFlags::LEVEL | IcrLowFlags::DESTINATION_SHORTHAND_ALL_INCLUDING_SELF;
            self.write(LapicRegister::ICR_LOW, icr_low_flags.bits());
            const DS: u32 = 1 << 12;
            while self.read(LapicRegister::ICR_LOW) & IcrLowFlags::DELIVERY_STATUS.bits() != 0 {} // wait for delivery status

            // 在APIC上启用中断（但不是在处理器上）
            self.write(LapicRegister::TPR, 0); // 通过设置TPR寄存器为0来允许接收所有优先级的中断
        }
    }

    // 获取当前APIC的ID
    fn id(&self) -> u32 {
        unsafe { self.read(LapicRegister::LAPIC_ID) >> 24 }
    }

    // 获取当前APIC的版本号
    fn version(&self) -> u32 {
        unsafe { self.read(LapicRegister::LAPIC_VERSION) }
    }

    // 获取当前APIC的中断命令寄存器（ICR）值
    fn icr(&self) -> u64 {
        unsafe { (self.read(LapicRegister::ICR_HIGH) as u64) << 32 | self.read(LapicRegister::ICR_LOW) as u64 }
    }

    // 设置中断命令寄存器（ICR）的值
    fn set_icr(&mut self, value: u64) {
        unsafe {
            // 等待当前的中断传递完成
            while self.read(LapicRegister::ICR_LOW).get_bit(12) {}
            // 设置ICR的高32位
            self.write(LapicRegister::ICR_HIGH, (value >> 32) as u32);
            // 设置ICR的低32位
            self.write(LapicRegister::ICR_LOW, value as u32);
            // 再次等待中断传递完成
            while self.read(LapicRegister::ICR_LOW).get_bit(12) {}
        }
    }

    // 发送一个End of Interrupt（EOI）信号
    fn eoi(&mut self) {
        unsafe {
            self.write(LapicRegister::EOI, 0);
        }
    }
}

// 实现Debug trait，为XApic提供格式化输出功能
impl Debug for XApic {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        f.debug_struct("Xapic")
         .field("id", &self.id())
         .field("version", &self.version())
         .field("icr", &self.icr())
         .finish()
    }
}
