mod apic;
mod syscall;
mod consts;
pub mod clock;
mod serial;
mod exceptions;

use apic::*;
use x86_64::structures::idt::InterruptDescriptorTable;
use crate::memory::physical_to_virtual;
use crate::interrupt::consts::Irq;

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        unsafe {
            exceptions::register_idt(&mut idt);
            clock::register_idt(&mut idt);
            serial::register_idt(&mut idt);
            syscall::register_idt(&mut idt);
        }
        idt
    };
}

pub fn init() {
    IDT.load(); // 加载中断描述表

    if XApic::support() {
        let mut apic = unsafe { XApic::new(LAPIC_ADDR) };
        apic.cpu_init(); // 初始化当前CPU的APIC
        // 如果需要，这里还可以添加更多的APIC配置代码

        // 启用特定的IRQ
        enable_irq(Irq::Serial0 as u8, 0); // enable IRQ4 for CPU0, 启用串口中断IRQ.
    } else {
        panic!("APIC not supported, cannot initialize interrupts.");
    }

    info!("Interrupts Initialized.");
}

#[inline(always)]
pub fn enable_irq(irq: u8, cpuid: u8) {
    let mut ioapic = unsafe { IoApic::new(physical_to_virtual(IOAPIC_ADDR)) };
    ioapic.enable(irq, cpuid);
}

#[inline(always)]
pub fn ack() {
    let mut lapic = unsafe { XApic::new(physical_to_virtual(LAPIC_ADDR)) };
    lapic.eoi();
}
