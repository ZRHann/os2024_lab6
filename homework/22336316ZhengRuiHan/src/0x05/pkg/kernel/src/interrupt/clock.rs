// 引入定义好的常量，可能包括中断向量号等
use super::consts::*;
use core::sync::atomic::{AtomicU64, Ordering};
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};
use crate::memory::gdt::CLOCK_INTERRUPT_IST_INDEX;
use crate::proc::ProcessContext;

// 定义一个函数，用于向IDT（中断描述符表）注册时钟中断处理程序
pub unsafe fn register_idt(idt: &mut InterruptDescriptorTable) {
    // 计算时钟中断的向量号并为其设置处理函数
    idt[Interrupts::IrqBase as u8 + Irq::Timer as u8]
        .set_handler_fn(clock_interrupt_handler)
        .set_stack_index(CLOCK_INTERRUPT_IST_INDEX as u16);
}

pub extern "C" fn clock_interrupt(mut context: ProcessContext) {
    // info!("Clock interrupt received.");
    // 调用 without_interrupts 以确保在操作过程中不会被其他中断打断
    x86_64::instructions::interrupts::without_interrupts(|| {
        // 调用进程调度切换函数
        crate::proc::switch(&mut context);
        super::ack();
    });
}
as_handler!(clock_interrupt);



// 使用AtomicU64定义COUNTER，初始值为0，确保原子操作
static COUNTER: AtomicU64 = AtomicU64::new(0);

// 读取COUNTER当前值
#[inline]
pub fn read_counter() -> u64 {
    COUNTER.load(Ordering::Relaxed)
}

// 增加COUNTER值，并返回新值
#[inline]
pub fn inc_counter() -> u64 {
    // 同样使用Relaxed顺序进行原子增加操作
    COUNTER.fetch_add(1, Ordering::Relaxed) + 1
}
