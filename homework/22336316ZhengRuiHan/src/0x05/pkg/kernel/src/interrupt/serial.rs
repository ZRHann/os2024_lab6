use super::consts::*;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};
use crate::drivers::serial::get_serial;
use crate::drivers::input::push_key;
use crate::serial::get_serial_for_sure;
pub unsafe fn register_idt(idt: &mut InterruptDescriptorTable) {
    idt[Interrupts::IrqBase as u8 + Irq::Serial0 as u8]
        .set_handler_fn(serial_handler);
}

pub extern "x86-interrupt" fn serial_handler(_st: InterruptStackFrame) {
    // info!("Serial interrupt received.");
    receive();
    super::ack();
}

fn receive() {
    // 尝试获取串口的锁，如果失败则panic
    // 试图从串口接收一个字节
    while let Some(data) = get_serial_for_sure().receive() {
        push_key(data); // 成功读取到数据，使用 push_key 将其放入输入缓冲区
        // 因为 push_key 内部已经处理了缓冲区满的情况，这里不需要额外的错误处理
    }
    // 注意：这里没有处理接收错误的情况
}
