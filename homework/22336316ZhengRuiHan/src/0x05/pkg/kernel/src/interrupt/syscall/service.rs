use core::alloc::Layout;

use crate::get_uefi_runtime;
use crate::proc::*;
use crate::utils::*;
use crate::proc;
use super::SyscallArgs;
use uefi::prelude::*;
use chrono::naive::NaiveDate;
use chrono::naive::NaiveDateTime;
use x86::current;
use crate::proc::{Semaphore, SemaphoreId, SemaphoreResult, SemaphoreSet};
use crate::proc::ProgramStatus;
pub fn fork_process(args: &SyscallArgs, context: &mut ProcessContext) {
    // fork current process, return pid
    // info!("fork: current context: {:?}", context);
    proc::fork(context);
}

pub fn spawn_process(args: &SyscallArgs) -> usize {
    // get app name by args
    //       - core::str::from_utf8_unchecked
    //       - core::slice::from_raw_parts
    let name = unsafe { 
        core::str::from_utf8_unchecked(
            core::slice::from_raw_parts(args.arg0 as *const u8, args.arg1)
        )
    };
    // spawn the process by name, handle spawn error, 
    // return 0 if failed, return pid as usize
    match proc::spawn(name) {
        Some(pid) => pid.0 as usize,
        None => 0,
    }
}

pub fn sys_write(args: &SyscallArgs) -> usize {
    // debug!("sys_write: {:?}", args);
    // fd: arg0 as u8, buf: &[u8] (ptr: arg1 as *const u8, len: arg2)
    
    // get buffer and fd by args
    // - core::slice::from_raw_parts
    let fd = args.arg0 as u8;
    let buf = unsafe { core::slice::from_raw_parts(args.arg1 as *const u8, args.arg2) };
    
    // call proc::write -> isize
    let ret = proc::write(fd, buf);

    // return the result as usize
    ret as usize
}

pub fn sys_read(args: &SyscallArgs) -> usize {
    // just like sys_write
    let fd = args.arg0 as u8;
    let buf = unsafe { core::slice::from_raw_parts_mut(args.arg1 as *mut u8, args.arg2) };

    // call proc::read -> isize
    let ret = proc::read(fd, buf);
    // if ret.is_positive() {
    //     info!("sys_read: {:?}", ret);
    // }
    // return the result as usize
    ret as usize
}

pub fn exit_process(args: &SyscallArgs, context: &mut ProcessContext) {
    // exit process with retcode
    proc::exit(args.arg0 as isize, context);
}

pub fn list_process() {
    // list all processes
    proc::print_process_list();
}

pub fn sys_allocate(args: &SyscallArgs) -> usize {
    let layout = unsafe { (args.arg0 as *const Layout).as_ref().unwrap() };
    if layout.size() == 0 {
        return 0;
    }
    let ret = crate::memory::user::USER_ALLOCATOR
        .lock()
        .allocate_first_fit(*layout);
    match ret {
        Ok(ptr) => ptr.as_ptr() as usize,
        Err(_) => 0,
    }
}

pub fn sys_deallocate(args: &SyscallArgs) {
    let layout = unsafe { (args.arg1 as *const Layout).as_ref().unwrap() };
    if args.arg0 == 0 || layout.size() == 0 {
        return;
    }
    let ptr = args.arg0 as *mut u8;
    unsafe {
        crate::memory::user::USER_ALLOCATOR
            .lock()
            .deallocate(core::ptr::NonNull::new_unchecked(ptr), *layout);
    }
}

pub fn sys_wait_pid(args: &SyscallArgs, context: &mut ProcessContext) {
    // block current process, wait for process with pid
    // return the exit code of the process if exited
    // return -1 if process does not exist
    // return -2 if process is not exited yet
    let pid = args.arg0 as u16;
    let manager = get_process_manager();
    match manager.get_proc(&ProcessId(pid)) {
        Some(proc) => {
            proc::wait_pid(ProcessId(pid), context);
        },
        None => {
            context.set_rax(usize::MAX - 1); // -1
        }
    }
}

pub fn sys_time(args: &SyscallArgs) -> usize {
    // 获取 UEFI 运行时服务
    let uefi_runtime = get_uefi_runtime().unwrap();
    // 获取当前时间
    let uefi_time = uefi_runtime.get_time().unwrap();
    // info!("UEFI Time: year={}, month={}, day={}, hour={}, minute={}, second={}, nanosecond={}",
    //     uefi_time.year(),
    //     uefi_time.month(),
    //     uefi_time.day(),
    //     uefi_time.hour(),
    //     uefi_time.minute(),
    //     uefi_time.second(),
    //     uefi_time.nanosecond()
    // );

    // 将 UEFI 时间转换为 NaiveDateTime
    let naive_date_time = NaiveDate::from_ymd_opt(
        uefi_time.year() as i32,
        uefi_time.month() as u32,
        uefi_time.day() as u32,
    ).unwrap().and_hms_micro_opt(
        uefi_time.hour() as u32,
        uefi_time.minute() as u32,
        uefi_time.second() as u32,
        uefi_time.nanosecond() / 1000,
    ).unwrap();
    // 返回秒级时间戳
    naive_date_time.and_utc().timestamp() as usize
}

pub fn sys_sem(args: &SyscallArgs, context: &mut ProcessContext) {
    match args.arg0 {
        0 => context.set_rax(new_sem(args.arg1 as u32, args.arg2)),
        1 => context.set_rax(remove_sem(args.arg1 as u32)),
        2 => sem_signal(args.arg1 as u32, context),
        3 => sem_wait(args.arg1 as u32, context),
        _ => context.set_rax(usize::MAX),
    }
}

pub fn sem_wait(key: u32, context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        let pid = current_pid();
        let ret = manager.current().write().sem_wait(key, pid);
        match ret {
            SemaphoreResult::Ok => context.set_rax(0),
            SemaphoreResult::NotExist => context.set_rax(1),
            SemaphoreResult::Block(pid) => {
                // FIXME: save, block it, then switch to next
                manager.save_current(context);
                manager.current().write().block();
                // manager.block_current();
                manager.switch_next(context);
            }
            _ => unreachable!(),
        }
    })
}

pub fn sem_signal(key: u32, context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        let ret = manager.current().write().sem_signal(key);
        match ret {
            SemaphoreResult::Ok => context.set_rax(0),
            SemaphoreResult::NotExist => context.set_rax(1),
            SemaphoreResult::WakeUp(pid) => {
                // wake up the process
                manager.get_proc(&pid).unwrap().write().resume();
                manager.push_ready(pid);
                context.set_rax(0);
            }
            _ => unreachable!(),
        }
    })
}

pub fn new_sem(key: u32, value: usize) -> usize {
    let manager = get_process_manager();
    if manager.current().write().sem_insert(key, value) {
        0
    } else {
        1
    }
}

pub fn remove_sem(key: u32) -> usize {
    let manager = get_process_manager();
    if manager.current().write().sem_remove(key) {
        0
    } else {
        1
    }
}

