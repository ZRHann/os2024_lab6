#![no_std]
#![allow(dead_code)]
#![feature(naked_functions)]
#![feature(abi_x86_interrupt)]
#![feature(alloc_error_handler)]
#![feature(type_alias_impl_trait)]
#![feature(panic_info_message)]
#![feature(map_try_insert)]
#![allow(clippy::missing_safety_doc)]
#![allow(clippy::result_unit_err)]

extern crate alloc;
#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate bitflags;
extern crate libm;

#[macro_use]
pub mod utils;
pub use utils::*;

#[macro_use]
pub mod drivers;
pub use drivers::*;

pub mod memory;
pub mod interrupt;
pub mod proc;

pub use alloc::format;
use boot::BootInfo;
use uefi::table::runtime::RuntimeServices;
use spin::Mutex;
use lazy_static::lazy_static;

lazy_static! {
    static ref UEFI_RUNTIME: Mutex<Option<&'static RuntimeServices>> = Mutex::new(None);
}

pub fn init(boot_info: &'static BootInfo) {
    serial::init(); // init serial output
    logger::init(boot_info.log_level); // init logger system
    memory::address::init(boot_info);
    memory::gdt::init(); // init gdt
    memory::allocator::init(); // init kernel heap allocator
    interrupt::init(); // init interrupts
    memory::init(boot_info); // init memory manager
    memory::user::init();
    proc::init(boot_info); // init process manager
    
    // 初始化 UEFI_RUNTIME
    let system_table = &boot_info.system_table;
    let runtime_services = unsafe { system_table.runtime_services() };
    *UEFI_RUNTIME.lock() = Some(runtime_services);

    x86_64::instructions::interrupts::enable();
    info!("Interrupts Enabled.");
    info!("YatSenOS initialized.");
}

pub fn shutdown(boot_info: &'static BootInfo) -> ! {
    info!("YatSenOS shutting down.");
    unsafe {
        boot_info.system_table.runtime_services().reset(
            boot::ResetType::SHUTDOWN,
            boot::UefiStatus::SUCCESS,
            None,
        );
    }
}

fn humanized_size(size: u64) -> (f64, &'static str) {
    if size < 1024 {
        return (size as f64, "B");
    } else if size < 1024 * 1024 {
        return (size as f64 / 1024.0, "KiB");
    } else if size < 1024 * 1024 * 1024 {
        return (size as f64 / 1024.0 / 1024.0, "MiB");
    } else {
        return (size as f64 / 1024.0 / 1024.0 / 1024.0, "GiB");
    }
}

pub fn wait(init: proc::ProcessId) {
    loop {
        if proc::still_alive(init) {
            // Why? Check reflection question 5
            x86_64::instructions::hlt();
        } else {
            break;
        }
    }
}

pub fn get_uefi_runtime() -> Option<&'static RuntimeServices> {
    *UEFI_RUNTIME.lock()
}
