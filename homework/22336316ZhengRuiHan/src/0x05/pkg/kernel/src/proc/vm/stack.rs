use x86_64::{
    structures::paging::{mapper::MapToError, page::{self, *}, Page},
    VirtAddr,
};


use super::{FrameAllocatorRef, MapperRef};

// 0xffff_ff00_0000_0000 is the kernel's address space
pub const STACK_MAX: u64 = 0x4000_0000_0000;
pub const STACK_MAX_PAGES: u64 = 0x100000;
pub const STACK_MAX_SIZE: u64 = STACK_MAX_PAGES * crate::memory::PAGE_SIZE;
pub const STACK_START_MASK: u64 = !(STACK_MAX_SIZE - 1);
// [bot..0x2000_0000_0000..top..0x3fff_ffff_ffff]
// init stack
pub const STACK_DEF_BOT: u64 = STACK_MAX - STACK_MAX_SIZE;
pub const STACK_DEF_PAGE: u64 = 1;
pub const STACK_DEF_SIZE: u64 = STACK_DEF_PAGE * crate::memory::PAGE_SIZE;

pub const STACK_INIT_BOT: u64 = STACK_MAX - STACK_DEF_SIZE;
pub const STACK_INIT_TOP: u64 = STACK_MAX - 8;

pub const STACK_INIT_PAGE: Page<Size4KiB> = Page::containing_address(VirtAddr::new(STACK_INIT_BOT));
pub const STACK_INIT_TOP_PAGE: Page<Size4KiB> = Page::containing_address(VirtAddr::new(STACK_INIT_TOP));

// [bot..0xffffff0100000000..top..0xffffff01ffffffff]
// kernel stack
pub const KSTACK_MAX: u64 = 0xffff_ff02_0000_0000;
pub const KSTACK_DEF_BOT: u64 = KSTACK_MAX - STACK_MAX_SIZE;
pub const KSTACK_DEF_PAGE: u64 = 1;
pub const KSTACK_DEF_SIZE: u64 = KSTACK_DEF_PAGE * crate::memory::PAGE_SIZE;

pub const KSTACK_INIT_BOT: u64 = KSTACK_MAX - KSTACK_DEF_SIZE;
pub const KSTACK_INIT_TOP: u64 = KSTACK_MAX - 8;

const KSTACK_INIT_PAGE: Page<Size4KiB> = Page::containing_address(VirtAddr::new(KSTACK_INIT_BOT));
const KSTACK_INIT_TOP_PAGE: Page<Size4KiB> =
    Page::containing_address(VirtAddr::new(KSTACK_INIT_TOP));
#[derive(Clone)]
pub struct Stack {
    range: PageRange<Size4KiB>,
}

impl Stack {
    pub fn new(top: Page, size: u64) -> Self {
        Self {
            range: Page::range(top - size + 1, top + 1),
        }
    }

    pub const fn empty() -> Self {
        Self {
            range: Page::range(STACK_INIT_TOP_PAGE, STACK_INIT_TOP_PAGE),
        }
    }

    pub const fn kstack() -> Self {
        Self {
            range: Page::range(KSTACK_INIT_PAGE, KSTACK_INIT_TOP_PAGE),
        }
    }

    pub fn init(&mut self, mapper: MapperRef, alloc: FrameAllocatorRef) {
        self.range = elf::map_range(STACK_INIT_BOT, STACK_DEF_PAGE, mapper, alloc, true, false).unwrap();
    }

    pub fn kernel_init(&mut self, mapper: MapperRef, alloc: FrameAllocatorRef) {
        self.range = elf::map_range(KSTACK_INIT_BOT, KSTACK_DEF_PAGE, mapper, alloc, false, false).unwrap();
    }

    pub fn handle_page_fault(
        &mut self,
        addr: VirtAddr,
        mapper: MapperRef,
        alloc: FrameAllocatorRef,
    ) -> bool {
        if !self.is_on_stack(addr) {
            info!("handle_page_fault: Address is not on stack: {:#x}", addr.as_u64());
            return false;
        }

        if let Err(m) = self.grow_stack(addr, mapper, alloc) {
            error!("Grow stack failed: {:?}", m);
            return false;
        }

        true
    }

    fn is_on_stack(&self, addr: VirtAddr) -> bool {
        let addr = addr.as_u64();
        let cur_stack_bot = self.range.start.start_address().as_u64();
        trace!("Current stack bot: {:#x}", cur_stack_bot);
        trace!("Address to access: {:#x}", addr);
        addr & STACK_START_MASK == cur_stack_bot & STACK_START_MASK
    }

    fn grow_stack(
        &mut self,
        addr: VirtAddr,
        mapper: MapperRef,
        alloc: FrameAllocatorRef,
    ) -> Result<(), MapToError<Size4KiB>> {
        debug_assert!(self.is_on_stack(addr), "Address is not on stack.");

        // grow stack for page fault
        let start_page = Page::<Size4KiB>::containing_address(addr);
        let start_addr = start_page.start_address().as_u64();
        let page_count = self.range.start - start_page;
        elf::map_range(
            start_addr, 
            page_count, 
            mapper, 
            alloc, 
            true, // TODO: 对内核栈来说，这里应该是false
            false
        ).expect("grow_stack: Stack mapping failed");
        self.range.start = start_page;
        Ok(())
    }

    pub fn memory_usage(&self) -> u64 {
        self.range.count() as u64 * crate::memory::PAGE_SIZE
    }

    pub fn range(&self) -> PageRange<Size4KiB> {
        self.range.clone()
    }

    pub fn fork(
        &self,
        mapper: MapperRef,
        alloc: FrameAllocatorRef,
        stack_offset_count: u64, // in addr, not page
    ) -> Self {
        // alloc & map new stack for child (see instructions)
        let new_stack_start_addr = self.range.start.start_address().as_u64() - stack_offset_count;
        let page_count = self.range.count() as u64;
        let new_stack = elf::map_range(
            new_stack_start_addr,
            page_count,
            mapper,
            alloc,
            true,
            false,
        ).expect("fork: Stack mapping failed");
        debug!("Forked stack: {:?}", new_stack);
        // copy the *entire stack* from parent to child
        elf::clone_range(
            self.range.start.start_address().as_u64(), 
            new_stack_start_addr,
            page_count
        );

        // return the new stack
        Self {
            range: new_stack,   
        }
    }
}

impl core::fmt::Debug for Stack {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        f.debug_struct("Stack")
            .field(
                "top", 
                &format_args!("{:#x}", self.range.end.start_address().as_u64()),
            )
            .field(
                "bot",
                &format_args!("{:#x}", self.range.start.start_address().as_u64()),
            )
            .finish()
    }
}