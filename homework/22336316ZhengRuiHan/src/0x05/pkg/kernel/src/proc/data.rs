// 引入所需的库和模块，包括BTreeMap用于存储环境变量，Arc和RwLock用于多线程中的安全和效率，以及页表处理的相关结构。
use alloc::{collections::BTreeMap, sync::Arc};
use spin::RwLock;
use sync::*;
use x86_64::structures::paging::{
    page::{PageRange, PageRangeInclusive},
    Page,
};
use crate::utils::resource::*;
// 引入父级模块或兄弟模块的内容。
use super::*;

// 定义ProcessData结构，用于存储进程的环境变量和栈段信息。
#[derive(Debug, Clone)]
pub struct ProcessData {
    // 使用Arc和RwLock封装的BTreeMap，用于存储和管理进程的环境变量，支持多线程安全访问。
    pub(super) env: Arc<RwLock<BTreeMap<String, String>>>,

    // 文件描述符表
    pub(super) resources: Arc<RwLock<ResourceSet>>,

    pub(super) semaphores: Arc<RwLock<SemaphoreSet>>,
}

// 为ProcessData实现Default trait，提供创建新ProcessData实例的默认方式。
impl Default for ProcessData {
    fn default() -> Self {
        Self {
            // 创建一个新的空BTreeMap，并用Arc和RwLock包裹，以实现线程安全的读写。
            env: Arc::new(RwLock::new(BTreeMap::new())),
            resources: Arc::new(RwLock::new(ResourceSet::default())), 
            semaphores: Arc::new(RwLock::new(SemaphoreSet::default())),
        }
    }
}

// ProcessData的实现部分。
impl ProcessData {
    // 提供一个静态方法new，用于创建带有默认值的ProcessData实例。
    pub fn new() -> Self {
        Self::default()
    }

    // 提供一个方法env，用于查询给定键的环境变量值，如果存在则返回Some，否则返回None。
    pub fn env(&self, key: &str) -> Option<String> {
        self.env.read().get(key).cloned()
    }

    // 提供一个方法set_env，用于设置环境变量的键值对，将新值写入BTreeMap中。
    pub fn set_env(&mut self, key: &str, val: &str) {
        self.env.write().insert(key.into(), val.into());
    }

    pub fn read(&self, fd: u8, buf: &mut [u8]) -> isize {
        self.resources.read().read(fd, buf)
    }
    
    pub fn write(&self, fd: u8, buf: &[u8]) -> isize {
        self.resources.read().write(fd, buf)
    }
}
