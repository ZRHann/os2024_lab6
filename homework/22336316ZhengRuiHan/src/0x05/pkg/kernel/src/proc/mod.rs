// 引入操作系统的不同模块，如上下文管理、数据管理、内存分页等。
mod context;
mod data;
mod manager;
mod paging;
mod pid;
mod process;
mod processor;
mod sync;
mod vm;
use alloc::sync::Arc;
use alloc::vec::Vec;
// 使用导入的模块中的特定功能。
use manager::*;
use process::*;
use boot::config::DEFAULT_CONFIG;
use crate::memory::PAGE_SIZE;
use crate::proc::vm::ProcessVm;
use xmas_elf::ElfFile;

// 分配和使用Rust标准库中的String类型。
use alloc::string::{String, ToString};
// 引入进程上下文相关的数据和函数。
pub use context::ProcessContext;
pub use paging::PageTableContext;
pub use data::ProcessData;
pub use pid::ProcessId;
pub use manager::{get_process_manager};
pub use sync::*;
pub use processor::current_pid;

// 引入x86_64架构特定的页面错误码处理。
use x86_64::structures::idt::PageFaultErrorCode;
use x86_64::VirtAddr;


// 定义内核进程的进程ID。
pub const KERNEL_PID: ProcessId = ProcessId(1);

// 定义进程状态的枚举类型。
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ProgramStatus {
    Running,
    Ready,
    Blocked,
    Dead,
}

/// 初始化进程管理器，配置内核进程数据和进程。
pub fn init(boot_info: &'static boot::BootInfo) {
    // kernel process
    let kproc = {
        let kernel_name = String::from("init"); // 定义内核进程的名字
        let parent = None; // 内核进程没有父进程
        let kproc_vm = ProcessVm::new(PageTableContext::new()).init_kernel_vm();
        let kproc_data = ProcessData::new();
        Process::new(kernel_name, parent, Some(kproc_vm), Some(kproc_data)) // 创建内核进程
    };
    let app_list = boot_info.loaded_apps.as_ref().unwrap();
    manager::init(kproc, app_list);
    info!("Process Manager Initialized.");
}

// 切换当前执行的进程上下文。
pub fn switch(context: &mut ProcessContext) {
    // debug!("Switching process context");
    x86_64::instructions::interrupts::without_interrupts(|| {
        // switch to the next process
        // save current process's context
        let proc_manager = get_process_manager();
        proc_manager.save_current(context);
        // handle ready queue update
        if proc_manager.current().write().status() == ProgramStatus::Ready {
            proc_manager.push_ready(proc_manager.current().pid());
        }
        // restore next process's context
        proc_manager.switch_next(context);
    });
}

// 用于生成内核线程的函数。
// pub fn spawn_kernel_thread(entry: fn() -> !, name: String, data: Option<ProcessData>) -> ProcessId {
//     x86_64::instructions::interrupts::without_interrupts(|| {
//         let entry = VirtAddr::new(entry as usize as u64);
//         get_process_manager().spawn_kernel_thread(entry, name, data)
//     })
// }
// 打印当前所有进程的列表。
pub fn print_process_list() {
    x86_64::instructions::interrupts::without_interrupts(|| {
        get_process_manager().print_process_list();
    })
}

// 获取当前进程的环境变量。
pub fn env(key: &str) -> Option<String> {
    x86_64::instructions::interrupts::without_interrupts(|| {
        // get current process's environment variable
        get_process_manager().current().read().env(key)
    })
}

// 结束当前进程并退出。
pub fn process_exit(ret: isize) -> ! {
    x86_64::instructions::interrupts::without_interrupts(|| {
        get_process_manager().kill_current(ret);
    });

    loop {
        x86_64::instructions::hlt();
    }
}

// 处理页面错误。
pub fn handle_page_fault(addr: VirtAddr, err_code: PageFaultErrorCode) -> bool {
    x86_64::instructions::interrupts::without_interrupts(|| {
        get_process_manager().handle_page_fault(addr, err_code)
    })
}


pub fn list_app() {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let app_list = get_process_manager().app_list();
        if app_list.is_none() {
            println!("[!] No app found in list!");
            return;
        }

        let apps = app_list
            .unwrap()
            .iter()
            .map(|app| app.name.as_str())
            .collect::<Vec<&str>>()
            .join(", ");

        // TODO: print more information like size, entry point, etc.
        println!("[+] App list: {}", apps);
    });
}

pub fn spawn(name: &str) -> Option<ProcessId> {
    let app = x86_64::instructions::interrupts::without_interrupts(|| {
        let app_list = get_process_manager().app_list()?;
        app_list.iter().find(|&app| app.name.eq(name))
    })?;

    elf_spawn(name.to_string(), &app.elf)
}

pub fn elf_spawn(name: String, elf: &ElfFile) -> Option<ProcessId> {
    let pid = x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        let process_name = name.to_lowercase();
        let parent = Arc::downgrade(&manager.current());
        let proc_data = ProcessData::new();
        let pid = manager.spawn(elf, name, Some(parent), Some(proc_data));

        pid
    });

    Some(pid)
}


pub fn fork(context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        // save_current as parent
        manager.save_current(context);
        // fork to get child
        let child_pid = manager.fork();
        // info!("fork: parent context: {:?}", manager.current().read().get_context());
        // info!("fork: child context: {:?}", manager.get_proc(&child_pid).unwrap().read().get_context());
        // push to child & parent to ready queue
        if manager.current().write().status() == ProgramStatus::Ready {
            manager.push_ready(manager.current().pid());
        }
        if manager.get_proc(&child_pid).unwrap().write().status() == ProgramStatus::Ready {
            manager.push_ready(child_pid);
        }
        // switch to next process
        manager.switch_next(context);
    })
}

pub fn read(fd: u8, buf: &mut [u8]) -> isize {
    x86_64::instructions::interrupts::without_interrupts(|| get_process_manager().current().read().proc_data().read(fd, buf))
}

pub fn write(fd: u8, buf: &[u8]) -> isize {
    x86_64::instructions::interrupts::without_interrupts(|| get_process_manager().current().read().proc_data().write(fd, buf))
}

pub fn exit(ret: isize, context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        manager.kill_self(ret);
        manager.switch_next(context);
    })
}

#[inline]
pub fn still_alive(pid: ProcessId) -> bool {
    x86_64::instructions::interrupts::without_interrupts(|| {
        // check if the process is still alive
        get_process_manager().get_exit_code(pid).is_none() 
    })
}

pub fn wait_pid(pid: ProcessId, context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        if let Some(ret) = manager.get_exit_code(pid) {
            context.set_rax(ret.try_into().unwrap()); // return the exit code of the process if exited
        } else {
            context.set_rax(usize::MAX - 2); // return -2 if process is not exited yet. return in WAITER's context
            manager.wait_pid(pid);
            manager.save_current(context);
            manager.current().write().block();
            // debug!("context1: {:?}", context);
            manager.switch_next(context);
            // manager.print_process_list();
            // debug!("context2: {:?}", context);
            // let proc = manager.get_proc(&pid).unwrap();
            // debug!("process #{:?}: \nstatus: {:?}", pid, proc.read().status());
        }
    })
}
