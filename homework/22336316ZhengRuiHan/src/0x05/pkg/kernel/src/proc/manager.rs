use super::*;
use crate::memory::{
    self,
    allocator::{ALLOCATOR, HEAP_SIZE},
    get_frame_alloc_for_sure, PAGE_SIZE,
};
use alloc::{borrow::ToOwned, collections::*, format, sync::Arc};
use alloc::sync::Weak;
use spin::{Mutex, RwLock};
use arrayvec::ArrayVec;

pub static PROCESS_MANAGER: spin::Once<ProcessManager> = spin::Once::new();

pub fn init(init: Arc<Process>, app_list: boot::AppListRef) {

    // set init process as Running
    init.write().resume();

    // set processor's current pid to init's pid
    processor::set_pid(init.pid());

    PROCESS_MANAGER.call_once(|| ProcessManager::new(init, app_list));
}

pub fn get_process_manager() -> &'static ProcessManager {
    PROCESS_MANAGER
        .get()
        .expect("Process Manager has not been initialized")
}

pub struct ProcessManager {
    processes: RwLock<BTreeMap<ProcessId, Arc<Process>>>,
    ready_queue: Mutex<VecDeque<ProcessId>>,
    app_list: boot::AppListRef,
    wait_queue: Mutex<BTreeMap<ProcessId, BTreeSet<ProcessId>>>, 
    // BTreeMap 的键值对应于被等待的进程 ID，BTreeSet 用于存储等待的进程 ID 的集合。
}

impl ProcessManager {
    pub fn new(init: Arc<Process>, app_list: boot::AppListRef) -> Self {
        let mut processes = BTreeMap::new();
        let ready_queue = VecDeque::new();
        let pid = init.pid();

        trace!("Init {:#?}", init);

        processes.insert(pid, init);
        Self {
            processes: RwLock::new(processes),
            ready_queue: Mutex::new(ready_queue),
            app_list: app_list, 
            wait_queue: Mutex::new(BTreeMap::new()),
        }
    }

    #[inline]
    pub fn push_ready(&self, pid: ProcessId) {
        self.ready_queue.lock().push_back(pid);
    }

    #[inline]
    fn add_proc(&self, pid: ProcessId, proc: Arc<Process>) {
        self.processes.write().insert(pid, proc);
    }

    #[inline]
    pub fn get_proc(&self, pid: &ProcessId) -> Option<Arc<Process>> {
        self.processes.read().get(pid).cloned()
    }

    pub fn current(&self) -> Arc<Process> {
        self.get_proc(&processor::current_pid())
            .expect("No current process")
    }

    pub fn save_current(&self, context: &ProcessContext) {
        // update current process's tick count
        self.current().write().tick();
        // save current process's context
        self.current().write().save(context);
    }

    pub fn switch_next(&self, context: &mut ProcessContext) -> ProcessId {
        // fetch the next process from ready queue
        let mut next_pid = self.ready_queue.lock().pop_front().unwrap();
        while self.get_proc(&next_pid).unwrap().read().status() == ProgramStatus::Dead {
            next_pid = self.ready_queue.lock().pop_front().unwrap();
        }
        // debug!("next_pid: {:?}\r", next_pid);
        // restore next process's context
        self.get_proc(&next_pid).unwrap().write().restore(context);

        // update processor's current pid
        processor::set_pid(next_pid);
        // if next_pid.0 == 4 {  
        //     info!("switch_next: next_pid: {:?}; context: {:#?}", next_pid, context);
        //     panic!();
        // }

        // return next process's pid
        next_pid
    }

    pub fn kill_current(&self, ret: isize) {
        self.kill(processor::current_pid(), ret);
    }

    pub fn handle_page_fault(&self, addr: VirtAddr, err_code: PageFaultErrorCode) -> bool {
        if err_code.contains(PageFaultErrorCode::PROTECTION_VIOLATION) {
            return false;
        }
        // let addr_value = addr.as_u64();
        // let fault_pid = ProcessId(((STACK_MAX - addr_value) / STACK_MAX_SIZE + 1) as u16);
        // debug!("fault_pid {:?}", fault_pid);
        // if fault_pid != processor::get_pid() {
        //     return false;
        // }
        // 根据PID查找进程
        let process = self.current();
        // debug!("process.pid() {:?}", process.pid());
        // debug!("addr {:?}", addr);
        // debug!("err_code {:?}", err_code);
        let res = process.write().handle_page_fault(addr); 
        res
    }

    pub fn kill(&self, pid: ProcessId, ret: isize) {
        let proc = self.get_proc(&pid);
        if proc.is_none() {
            warn!("Process #{} not found.", pid);
            return;
        }
        let proc = proc.unwrap();
        if proc.read().status() == ProgramStatus::Dead {
            warn!("Process #{} is already dead.", pid);
            return;
        }
        trace!("Kill {:#?}", &proc);
        proc.kill(ret);
        if let Some(pids) = self.wait_queue.lock().remove(&pid) {
            for pid in pids {
                self.wake_up(pid, Some(ret));
            }
        }
    }

    pub fn print_process_list(&self) {
        let mut output = String::from("  PID | PPID | Process Name |  Ticks  |  Mem  | Status  \n");

        self.processes
            .read()
            .values()
            .filter(|p| p.read().status() != ProgramStatus::Dead)
            .for_each(|p| output += format!("{}\n", p).as_str());

        // TODO: print memory usage of kernel heap

        output += format!("Queue  : {:?}\n", self.ready_queue.lock()).as_str();

        output += &processor::print_processors();

        print!("{}", output);
    }

    pub fn get_exit_code(&self, pid: ProcessId) -> Option<isize> {
        let processes = self.processes.read();
        if let Some(proc) = processes.get(&pid) {
            let proc = proc.read();
            if proc.status() == ProgramStatus::Dead {
                return proc.exit_code();
            }
        }
        None
    }

    pub fn app_list(&self) -> Option<&boot::AppList> {
        Some(self.app_list)
    }

    pub fn spawn(
        &self,
        elf: &ElfFile,
        name: String,
        parent: Option<Weak<Process>>,
        proc_data: Option<ProcessData>,
    ) -> ProcessId {
        let kproc = self.get_proc(&KERNEL_PID).unwrap();
        let kproc_vm = kproc.read().clone_vm();
        let entry = VirtAddr::new(elf.header.pt2.entry_point());
        let proc = Process::new(name, parent, Some(kproc_vm), proc_data);

        // alloc stack for the new process 
        let stack_top = proc.alloc_init_stack();

        // set the stack frame
        proc.write().get_context_mut().init_stack_frame(entry, stack_top);

        // load elf file to process
        let _ = proc.write().load_elf(elf);
        
        // add to process map
        let pid = proc.pid();
        // debug!("pid: {:?}", pid);
        // debug!("proc: {:?}", proc.get_context().stack_frame);
        self.add_proc(pid, proc);

        // push to ready queue
        self.push_ready(pid);
        
        // return new process pid
        pid
    }

    pub fn kill_self(&self, ret: isize) {
        self.kill(self.current().pid(), ret);
    }

    pub fn fork(&self) -> ProcessId {
        // get current process
        let parent = self.current();
        // fork to get child
        let child = parent.fork();
        let child_pid = child.pid();
        // add child to process list
        self.add_proc(child_pid, child);
        // print the process ready queue?
        child_pid
    }

    /// Block the process with the given pid
    pub fn block(&self, pid: ProcessId) {
        if let Some(proc) = self.get_proc(&pid) {
            proc.write().block();
        }
    }

    pub fn wait_pid(&self, pid: ProcessId) {
        let mut wait_queue = self.wait_queue.lock();
        // push the current process to the wait queue
        //   `processor::current_pid()` is waiting for `pid`
        let current_pid: ProcessId = processor::current_pid();
        if !wait_queue.contains_key(&pid) {
            wait_queue.insert(pid, BTreeSet::new());
        }
        wait_queue.get_mut(&pid).unwrap().insert(current_pid);
    }

    /// Wake up the process with the given pid
    ///
    /// If `ret` is `Some`, set the return value of the process
    pub fn wake_up(&self, pid: ProcessId, ret: Option<isize>) {
        if let Some(proc) = self.get_proc(&pid) {
            // let mut inner = proc.write();
            if let Some(ret) = ret {
                // set the return value of the process
                //        like `context.set_rax(ret as usize)`
                proc.write().set_rax(ret as usize);
            }
            // set the process as ready
            proc.write().resume();
            // push to ready queue
            self.push_ready(pid);
        }
    }
}
