use self::context::ProcessContextValue;
use self::vm::stack::STACK_MAX_SIZE;

use super::*;
use alloc::sync::{Arc, Weak};
use alloc::vec::Vec;
use spin::*;
use volatile::VolatileRef;
use x86_64::registers::control::*;
use x86_64::structures::paging::*;
use core::borrow::BorrowMut;
use core::clone;
use core::intrinsics::{copy_nonoverlapping, write_bytes};
use core::ops::{Deref, DerefMut};

#[derive(Clone)]
pub struct Process {
    pid: ProcessId,
    inner: Arc<RwLock<ProcessInner>>,
}

// #[derive(Clone)]
pub struct ProcessInner {
    pid: ProcessId,
    name: String,
    parent: Option<Weak<Process>>,
    children: Vec<Arc<Process>>,
    ticks_passed: usize,
    status: ProgramStatus,
    exit_code: Option<isize>,
    context: ProcessContext,
    proc_vm: Option<ProcessVm>,
    proc_data: Option<ProcessData>,
}

impl Process {
    #[inline]
    pub fn pid(&self) -> ProcessId {
        self.pid
    }

    #[inline]
    pub fn write(&self) -> RwLockWriteGuard<ProcessInner> {
        self.inner.write()
    }

    #[inline]
    pub fn read(&self) -> RwLockReadGuard<ProcessInner> {
        self.inner.read()
    }

    pub fn new(
        name: String,
        parent: Option<Weak<Process>>,
        proc_vm: Option<ProcessVm>, 
        proc_data: Option<ProcessData>,
    ) -> Arc<Self> {
        let name = name.to_ascii_lowercase();

        // create context
        let pid = ProcessId::new();

        let inner = ProcessInner {
            pid,
            name,
            parent,
            status: ProgramStatus::Ready,
            context: ProcessContext::default(),
            ticks_passed: 0,
            exit_code: None,
            children: Vec::new(),
            proc_data: proc_data,
            proc_vm: proc_vm,
        };

        trace!("New process {}#{} created.", &inner.name, pid);

        // create process struct
        Arc::new(Self {
            pid,
            inner: Arc::new(RwLock::new(inner)),
        })
    }

    pub fn kill(&self, ret: isize) {
        let mut inner: rwlock::RwLockWriteGuard<ProcessInner, Spin> = self.inner.write();

        debug!(
            "Killing process {}#{} with ret code: {}",
            inner.name(),
            self.pid,
            ret
        );

        inner.kill(ret);
    }

    pub fn alloc_init_stack(&self) -> VirtAddr {
        self.write().vm_mut().init_proc_stack(self.pid)
    }

    

    pub fn fork(self: &Arc<Self>) -> Arc<Self> {
        // lock inner as write
        // FIXME: inner fork with parent weak ref
        let parent_weak: Weak<Process> = Arc::downgrade(self);
        let child_inner = {
            let self_inner = self.inner.read();
            self_inner.fork(parent_weak)
        };
        let child_pid = child_inner.pid;

        // FOR DBG: maybe print the child process info
        //          e.g. parent, name, pid, etc.
        debug!("Forking process: parent_pid={}, name={}", self.pid(), self.inner.read().name());

        // make the arc of child
        let child_process = Arc::new(Process {
            pid: child_pid,
            inner: Arc::new(RwLock::new(child_inner)),
        });
        // add child to current process's children list
        self.inner.write().children.push(child_process.clone()); // Arc的clone, 增加引用计数，而不是深拷贝
        // set fork ret value for parent with `context.set_rax`
        self.inner.write().context.set_rax(child_pid.0.into());
        // mark the child as ready & return it
        child_process
    }
}

impl ProcessInner {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn proc_data(&self) -> &ProcessData {
        self.proc_data.as_ref().unwrap()
    }

    pub fn tick(&mut self) {
        self.ticks_passed += 1;
    }

    pub fn status(&self) -> ProgramStatus {
        self.status
    }

    pub fn pause(&mut self) {
        self.status = ProgramStatus::Ready;
    }

    pub fn resume(&mut self) {
        self.status = ProgramStatus::Running;
    }

    pub fn block(&mut self) {
        self.status = ProgramStatus::Blocked;
    }

    pub fn exit_code(&self) -> Option<isize> {
        self.exit_code
    }

    pub fn vm(&self) -> &ProcessVm {
        self.proc_vm.as_ref().unwrap()
    }

    pub fn vm_mut(&mut self) -> &mut ProcessVm {
        self.proc_vm.as_mut().unwrap()
    }

    pub fn clone_vm(&self) -> ProcessVm {
        let mut cloned_vm = ProcessVm::new(self.vm().page_table.clone_l4());
        cloned_vm.stack = self.vm().stack.clone();
        cloned_vm
    }

    pub fn is_ready(&self) -> bool {
        self.status == ProgramStatus::Ready
    }

    /// Save the process's context
    /// mark the process as ready
    pub(super) fn save(&mut self, context: &ProcessContext) {
        // save the process's context
        if self.status == ProgramStatus::Dead {
            return;
        }
        self.context.save(&context.clone());
        self.status = ProgramStatus::Ready;
    }

    /// Restore the process's context
    /// mark the process as running
    pub(super) fn restore(&mut self, context: &mut ProcessContext) {
        // restore the process's context
        // debug!("status {:?} Restoring process context", self.status());
        if self.status == ProgramStatus::Dead {
            return;
        }
        self.context.restore(context);
        
        self.status = ProgramStatus::Running;

        // restore the process's page table
        self.vm().page_table.load();
    } 

    pub fn parent(&self) -> Option<Arc<Process>> {
        self.parent.as_ref().and_then(|p| p.upgrade())
    }

    pub fn kill(&mut self, ret: isize) {
        // 回收进程栈段
        let start_addr = self.vm().stack.range().start.start_address().as_u64();
        let page_count = self.vm().stack.range().count() as u64;
        let _ = elf::unmap_range(
            start_addr, 
            page_count, 
            &mut self.vm().page_table.mapper(), 
            &mut *crate::memory::get_frame_alloc_for_sure(),
        );
        // take and drop unused resources
        self.proc_vm.take();
        self.proc_data.take();
        // set exit code
        self.exit_code = Some(ret);
        // set status to dead
        self.status = ProgramStatus::Dead;
    }

    pub fn handle_page_fault(&mut self, addr: VirtAddr) -> bool {
        self.vm_mut().handle_page_fault(addr)
    }

    pub fn load_elf(
        &mut self,
        elf: &ElfFile
    ) -> Result<(), &'static str> {
        // load elf to process pagetable
        let buf = elf.input;
        let mut page_table_mapper = self.vm().page_table.mapper();
        let frame_allocator = &mut *crate::memory::get_frame_alloc_for_sure();
        for segment in elf.program_iter() {
            if let Ok(xmas_elf::program::Type::Load) = segment.get_type() {
                let addr = segment.virtual_addr();
                let mem_size = segment.mem_size();
                let file_size = segment.file_size();
                let offset = segment.offset();
                let page_count = (mem_size + 4095) / 4096; // 向上取整至页的大小
        
                // 使用 map_range 函数映射内存区域
                let result = elf::map_range(addr, 
                    page_count, 
                    &mut page_table_mapper, 
                    frame_allocator, 
                    true, 
                    false, 
                );
                if let Err(e) = result {
                    panic!("Error mapping memory: {:?}", e);
                }
                // 计算目标内存的裸指针
                let dest_base = addr as *mut u8;
                // 保存当前页表，加载用户程序页表
                let (old_cr3_frame, old_cr3_flags) = Cr3::read();
                self.vm().page_table.load();
                // 从buf复制数据到新映射的内存
                unsafe {
                    copy_nonoverlapping(buf[offset as usize..].as_ptr(), 
                        dest_base, 
                        file_size as usize
                    );
                    // 如果 mem_size 大于 file_size，剩余的内存需要清零
                    if mem_size > file_size {
                        let extra_base = dest_base.add(file_size as usize);
                        write_bytes(extra_base, 0, (mem_size - file_size) as usize);
                    }
                }
                // 恢复页表
                unsafe {
                    Cr3::write(old_cr3_frame, old_cr3_flags);
                }
                // 更新代码段大小
                self.vm_mut().code_segment_size += (page_count * 4096) as u64;
                // debug!("code_segment_size: {:?}", self.proc_data().code_segment_size);
            }
        }
        Ok(())
    }
    
    pub fn fork(&self, parent: Weak<Process>) -> ProcessInner {
        // fork the process virtual memory struct
        let parent_arc = parent.upgrade().unwrap();
        let parent_inner = parent_arc.read();
        // calculate the real stack offset
        let child_count = parent_inner.children.len() as u64 + 1;
        let chlid_vm: Option<ProcessVm> = Some(parent_inner.vm().fork(child_count * STACK_MAX_SIZE));
        // update rsp, rbp
        let mut child_context = parent_inner.context.clone();
        child_context.stack_frame.stack_pointer -= child_count * STACK_MAX_SIZE;
        // child_context.regs.rbp -= child_count as usize * STACK_MAX_SIZE as usize;
        // set the return value 0 for child with `context.set_rax`
        child_context.set_rax(0);
        // clone the process data struct
        let child_data = Some(parent_inner.proc_data().clone());
        // construct the child process inner
        let child_inner = ProcessInner {
            pid: ProcessId::new(),
            name: parent_inner.name().to_string(),
            parent: Some(parent.clone()),
            children: Vec::new(),
            ticks_passed: 0,
            status: ProgramStatus::Ready,
            exit_code: None,
            context: child_context,
            proc_vm: chlid_vm,
            proc_data: child_data,
        };

        // NOTE: return inner because there's no pid record in inner
        child_inner
    }

    pub fn get_context(&self) -> ProcessContext {
        self.context.clone()
    }

    pub fn get_context_mut(&mut self) -> &mut ProcessContext {
        &mut self.context
    }
    
    pub fn sem_wait(&self, key: u32, pid: ProcessId) -> SemaphoreResult{
        self.proc_data().semaphores.write().wait(key, pid)
    }

    pub fn sem_signal(&self, key: u32) -> SemaphoreResult {
        self.proc_data().semaphores.write().signal(key)
    }

    pub fn sem_insert(&self, key: u32, value: usize) -> bool {
        self.proc_data().semaphores.write().insert(key, value)
    }

    pub fn sem_remove(&self, key: u32) -> bool {
        self.proc_data().semaphores.write().remove(key)
    }

    pub fn set_rax(&mut self, value: usize) {
        self.context.set_rax(value);
    }
}

impl core::ops::Deref for Process {
    type Target = Arc<RwLock<ProcessInner>>;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl core::ops::Deref for ProcessInner {
    type Target = ProcessData;
    fn deref(&self) -> &Self::Target {
        self.proc_data
            .as_ref()
            .expect("Process data empty. The process may be killed.")
    }
}

impl core::ops::DerefMut for ProcessInner {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.proc_data
            .as_mut()
            .expect("Process data empty. The process may be killed.")
    }
}

impl core::fmt::Debug for Process {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        let mut f = f.debug_struct("Process");
        f.field("pid", &self.pid);

        let inner = self.inner.read();
        f.field("name", &inner.name);
        f.field("parent", &inner.parent().map(|p| p.pid));
        f.field("status", &inner.status);
        f.field("ticks_passed", &inner.ticks_passed);
        f.field(
            "children",
            &inner.children.iter().map(|c| c.pid.0).collect::<Vec<u16>>(),
        );
        f.field("vm", &inner.proc_vm);
        f.field("status", &inner.status);
        f.field("context", &inner.context);
        f.finish()
    }
}

impl core::fmt::Display for Process {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        let inner = self.inner.read();
        let proc_stack_size = inner.vm().stack.memory_usage();
        write!(
            f,
            " #{:-3} | #{:-3} | {:12} | {:7} | {:5} | {:?}",
            self.pid.0,
            inner.parent().map(|p| p.pid.0).unwrap_or(0),
            inner.name,
            inner.ticks_passed,
            inner.vm().code_segment_size + proc_stack_size, 
            inner.status, 
        )?;
        Ok(())
    }
}
