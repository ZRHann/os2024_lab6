#![no_std]
#![no_main]

use ysos::{proc::{get_process_manager, list_app, print_process_list, ProcessId}, *};
use ysos_kernel as ysos;
extern crate alloc;

boot::entry_point!(kernel_main);

pub fn kernel_main(boot_info: &'static boot::BootInfo) -> ! {
    ysos::init(boot_info);
    ysos::wait(spawn_init());
    ysos::shutdown(boot_info);
}

pub fn spawn_init() -> proc::ProcessId {
    // NOTE: you may want to clear the screen before starting the shell
    print!("\x1b[1;1H\x1b[2J");
    proc::spawn("sh").unwrap()
}