// 引入Rust的宏和x86_64特定的结构和函数
use lazy_static::lazy_static;
use x86_64::registers::segmentation::Segment;
use x86_64::structures::gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector};
use x86_64::structures::tss::TaskStateSegment;
use x86_64::VirtAddr;

// 定义中断栈表（Interrupt Stack Table, IST）的索引常量，用于标识特定的中断类型
pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;
pub const PAGE_FAULT_IST_INDEX: u16 = 1;
pub const GENERAL_PROTECTION_FAULT_IST_INDEX: u16 = 2;
pub const CLOCK_INTERRUPT_IST_INDEX: u16 = 3;
pub const SYSCALL_IST_INDEX: u16 = 4;

// 定义IST的大小数组，每个栈的大小是0x1000（4KB）
pub const IST_SIZES: [usize; 5] = [0x1000, 0x1000, 0x1000, 0x1000, 0x1000];

// 使用lazy_static宏定义一个静态的TSS变量
lazy_static! {
    // TSS的类型为TaskStateSegment，这里进行初始化
    static ref TSS: TaskStateSegment = {
        // 新建一个TSS实例
        let mut tss = TaskStateSegment::new();

        // 初始化TSS的特权栈表，这里仅设置了第0个栈
        tss.privilege_stack_table[0] = {
            // 定义栈的大小
            const STACK_SIZE: usize = IST_SIZES[0];
            // 静态分配栈空间，初始值全为0
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
            // 获取栈起始地址
            let stack_start = VirtAddr::from_ptr(unsafe { STACK.as_ptr() });
            // 计算栈结束地址
            let stack_end = stack_start + STACK_SIZE as u64;
            // 打印栈的起始和结束地址
            info!(
                "Privilege Stack: 0x{:016x}-0x{:016x}",
                stack_start.as_u64(),
                stack_end.as_u64()
            );
            // 将栈结束地址作为栈顶地址返回
            stack_end
        };

        // 为Double Fault设置独立栈
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = IST_SIZES[DOUBLE_FAULT_IST_INDEX as usize];
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
            let stack_start = VirtAddr::from_ptr(unsafe { STACK.as_ptr() });
            let stack_end = stack_start + STACK_SIZE as u64;
            info!(
                "Double Fault Stack: 0x{:016x}-0x{:016x}",
                stack_start.as_u64(),
                stack_end.as_u64()
            );
            stack_end
        };

        // 为Page Fault设置独立栈
        tss.interrupt_stack_table[PAGE_FAULT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = IST_SIZES[PAGE_FAULT_IST_INDEX as usize];
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
            let stack_start = VirtAddr::from_ptr(unsafe { STACK.as_ptr() });
            let stack_end = stack_start + STACK_SIZE as u64;
            info!(
                "Page Fault Stack : 0x{:016x}-0x{:016x}",
                stack_start.as_u64(),
                stack_end.as_u64()
            );
            stack_end
        };
        
        // 为时钟中断设置独立栈
        tss.interrupt_stack_table[CLOCK_INTERRUPT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = IST_SIZES[CLOCK_INTERRUPT_IST_INDEX as usize];
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
            let stack_start = VirtAddr::from_ptr(unsafe { STACK.as_ptr() });
            let stack_end = stack_start + STACK_SIZE as u64;
            info!(
                "Clock Interrupt Stack: 0x{:016x}-0x{:016x}",
                stack_start.as_u64(),
                stack_end.as_u64()
            );
            stack_end
        };

        // 为系统调用中断设置独立栈
        tss.interrupt_stack_table[SYSCALL_IST_INDEX as usize] = {
            const STACK_SIZE: usize = IST_SIZES[SYSCALL_IST_INDEX as usize];
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
            let stack_start = VirtAddr::from_ptr(unsafe { STACK.as_ptr() });
            let stack_end = stack_start + STACK_SIZE as u64;
            info!(
                "Syscall Interrupt Stack: 0x{:016x}-0x{:016x}",
                stack_start.as_u64(),
                stack_end.as_u64()
            );
            stack_end
        };
        // 返回初始化好的TSS实例
        tss
    };
}

/*
GDT是一个内存中的表，它定义了系统中所有的内存段。
每个条目（称为段描述符）定义了一个段的基址、界限和一些与之相关的属性，
如访问权限、段的类型（代码段、数据段、系统段等）和其他的段特性（如DPL，描述符特权级）。
操作系统通过GDT来实现内存保护和控制程序的访问权限，确保不同的程序和用户之间的内存空间是隔离的。
*/
// 使用lazy_static宏定义一个静态的GDT变量和其选择器
lazy_static! {
    // GDT的类型为(GlobalDescriptorTable, KernelSelectors)，这里进行初始化
    static ref GDT: (GlobalDescriptorTable, KernelSelectors, UserSelectors) = {
        // 新建一个GDT实例
        let mut gdt = GlobalDescriptorTable::new();
        // 向GDT中添加内核、用户代码段、数据段描述符，并获取选择器
        let code_selector = gdt.append(Descriptor::kernel_code_segment());
        let data_selector = gdt.append(Descriptor::kernel_data_segment());
        let user_code_selector = gdt.append(Descriptor::user_code_segment());
        let user_data_selector = gdt.append(Descriptor::user_data_segment());
        // 向GDT中添加TSS段描述符，并获取选择器
        let tss_selector = gdt.append(Descriptor::tss_segment(&TSS));
        // 返回GDT实例和其选择器集合
        (
            gdt,
            KernelSelectors {
                code_selector,
                data_selector,
                tss_selector,
            },
            UserSelectors {
                user_code_selector,
                user_data_selector,
                tss_selector,
            }
        )
    };
}

// 定义内核选择器结构体，包含代码段、数据段和TSS选择器
#[derive(Debug)]
pub struct KernelSelectors {
    pub code_selector: SegmentSelector,
    pub data_selector: SegmentSelector,
    tss_selector: SegmentSelector,
}

// 定义用户选择器结构体，包含代码段、数据段和TSS选择器
#[derive(Debug)]
pub struct UserSelectors {
    pub user_code_selector: SegmentSelector,
    pub user_data_selector: SegmentSelector,
    tss_selector: SegmentSelector,
}

// 定义初始化函数，用于加载GDT并设置各种段寄存器
pub fn init() {
    use x86_64::instructions::segmentation::{CS, DS, ES, FS, GS, SS};
    use x86_64::instructions::tables::load_tss;
    use x86_64::PrivilegeLevel;

    // 加载GDT
    GDT.0.load();
    // 设置代码段、数据段和堆栈段寄存器
    unsafe {
        CS::set_reg(GDT.1.code_selector);
        DS::set_reg(GDT.1.data_selector);
        SS::set_reg(SegmentSelector::new(0, PrivilegeLevel::Ring0));
        ES::set_reg(SegmentSelector::new(0, PrivilegeLevel::Ring0));
        FS::set_reg(SegmentSelector::new(0, PrivilegeLevel::Ring0));
        GS::set_reg(SegmentSelector::new(0, PrivilegeLevel::Ring0));
        // 加载TSS
        load_tss(GDT.1.tss_selector);
    }

    // 计算内核IST的总大小，并以人类可读的格式打印
    let mut size = 0;
    for &s in IST_SIZES.iter() {
        size += s;
    }
    let (size, unit) = crate::humanized_size(size as u64);
    info!("Kernel IST Size  : {:>7.*} {}", 3, size, unit);

    // 打印GDT初始化完成的信息
    info!("GDT Initialized.");
}

// 定义获取选择器的函数，返回一个对KernelSelectors静态引用
pub fn get_selector() -> &'static KernelSelectors {
    &GDT.1
}

pub fn get_user_selector() -> &'static UserSelectors {
    &GDT.2
}