use core::{
    hint::spin_loop,
    sync::atomic::{AtomicBool, Ordering},
};

use crate::*;
use crate::syscall::*;

pub struct SpinLock {
    bolt: AtomicBool,
}

impl SpinLock {
    pub const fn new() -> Self {
        Self {
            bolt: AtomicBool::new(false), // true means locked
        }
    }

    pub fn acquire(&self) -> SpinLockGuard {
        // acquire the lock, spin if the lock is not available
        while self.bolt.compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed).is_err() {
            spin_loop(); // 提高性能，编译成pause指令。
        }
        SpinLockGuard { lock: self }
    }

    // fn release(&self) {
    //     // release the lock
    //     self.bolt.store(false, Ordering::Release);
    // }
}

pub struct SpinLockGuard<'a> {
    lock: &'a SpinLock,
}

// 离开作用域时，会自动调用drop方法解锁。
impl<'a> Drop for SpinLockGuard<'a> {
    fn drop(&mut self) {
        // self.lock.release();
        self.bolt.store(false, Ordering::Release);
    }
}

impl<'a> core::ops::Deref for SpinLockGuard<'a> {
    type Target = SpinLock;

    fn deref(&self) -> &Self::Target {
        &self.lock
    }
}

unsafe impl Sync for SpinLock {} // Why? Check reflection question 5

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Semaphore {
    key: u32, 
}

impl Semaphore {
    pub const fn new(key: u32) -> Self {
        Semaphore { key }
    }

    #[inline(always)]
    pub fn init(&self, value: usize) {
        sys_new_sem(self.key, value);
    }

    #[inline(always)]
    pub fn wait(&self) {
        sys_sem_wait(self.key);
    }

    #[inline(always)]
    pub fn signal(&self) {
        sys_sem_signal(self.key);
    }

    #[inline(always)]
    pub fn remove(&self) {
        sys_remove_sem(self.key);
    }
}
unsafe impl Sync for Semaphore {}

#[macro_export]
macro_rules! semaphore_array {
    [$($x:expr),+ $(,)?] => {
        [ $($crate::Semaphore::new($x),)* ]
    }
}
