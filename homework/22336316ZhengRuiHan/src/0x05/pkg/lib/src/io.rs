use core::arch::x86_64;

use crate::*;
use alloc::string::{String, ToString};
use alloc::vec::Vec;
use spin::Mutex;

pub struct Stdin {
}
pub struct Stdout;
pub struct Stderr;

// 检查字节序列是否构成了一个完整的UTF-8字符
fn is_complete_utf8_char(bytes: &[u8]) -> bool {
    match bytes.len() {
        1 => bytes[0] & 0x80 == 0, // 对于1字节的ASCII字符
        2 => bytes[0] & 0xE0 == 0xC0, // 对于2字节的字符
        3 => bytes[0] & 0xF0 == 0xE0, // 对于3字节的字符
        4 => bytes[0] & 0xF8 == 0xF0, // 对于4字节的字符
        _ => false,
    }
}

impl Stdin {
    fn new() -> Self {
        Self {
        }
    }

    pub fn read_line(&self) -> String {
        let mut line = String::with_capacity(128);
        let mut utf8_bytes = Vec::new();
        let mut buf = [0; 1];
    
        loop {
            // 从输入缓冲区读取一个字节
            if let Some(_) = sys_read(0, &mut buf) {
                utf8_bytes.push(buf[0]);
    
                // 检查是否收集到了完整的UTF-8字符
                if is_complete_utf8_char(&utf8_bytes) {
                    if let Ok(s) = core::str::from_utf8(&utf8_bytes) {
                        match s {
                            "\n" | "\r" => {
                                sys_write(1, b"\n");
                                break;
                            },
                            "\x08" | "\x7F" => { // 处理退格键
                                if !line.is_empty() {
                                    let last_utf8_len = line.pop().unwrap().len_utf8();
                                    let utf8_display_width = match last_utf8_len {
                                        1 => 1,
                                        2 => 2,
                                        3 => 2,
                                        4 => 2,
                                        _ => 0,
                                    };
                                    for _ in 0..utf8_display_width {
                                        sys_write(1, b"\x08 \x08"); // 从屏幕上删除一个字符
                                    }
                                }
                            },
                            _ => {
                                line.push_str(s);
                                sys_write(1, s.as_bytes()); // 将字符打印到屏幕上
                            },
                        }
                    }
                    utf8_bytes.clear(); // 清空缓冲区以接收下一个字符
                }
            }
        }
        line
    }
}

impl Stdout {
    fn new() -> Self {
        Self
    }

    pub fn write(&self, s: &str) {
        sys_write(1, s.as_bytes());
    }
}

impl Stderr {
    fn new() -> Self {
        Self
    }

    pub fn write(&self, s: &str) {
        sys_write(2, s.as_bytes());
    }
}

pub fn stdin() -> Stdin {
    Stdin::new()
}

pub fn stdout() -> Stdout {
    Stdout::new()
}

pub fn stderr() -> Stderr {
    Stderr::new()
}
