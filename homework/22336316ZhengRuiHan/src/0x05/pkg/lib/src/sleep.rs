use crate::sys_time;

pub fn sleep(secs: usize) {
    let start = sys_time();
    let mut current = start;
    while current - start < secs {
        current = sys_time();
    }
}