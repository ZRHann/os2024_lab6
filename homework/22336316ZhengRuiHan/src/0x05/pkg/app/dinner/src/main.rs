#![no_std]
#![no_main]

extern crate lib;
use lib::*;
use core::arch::asm;
use rand::prelude::*;
use rand_chacha::ChaCha20Rng;
const PHILOSOPHER_COUNT: usize = 5;
static CHOPSTICKS: [Semaphore; PHILOSOPHER_COUNT] = semaphore_array![0, 1, 2, 3, 4];

fn main() -> isize {
    for i in 0..PHILOSOPHER_COUNT {
        CHOPSTICKS[i].init(1);
    }
    let mut pids = [0u16; PHILOSOPHER_COUNT];
    for i in 0..PHILOSOPHER_COUNT {
        let pid = sys_fork();
        if pid == 0 {
            philosopher(i);
            sys_exit(0);
        } else {
            pids[i] = pid as u16;
        }
    }

    for i in 0..PHILOSOPHER_COUNT {
        sys_wait_pid(pids[i]);
    }

    0
}

fn philosopher(id: usize) {
    let left = id;
    let right = (id + 1) % PHILOSOPHER_COUNT;
    
    loop {
        think(id);
        // eat(id, left, right);
        better_eat(id, left, right);
    }
}

fn think(id: usize) {
    println!("Philosopher {} is thinking.", id);
    delay_random();
    // delay();
}

fn better_eat(id: usize, left: usize, right: usize) {
    println!("Philosopher {} want to eat.", id);
    let smaller = left.min(right);
    let larger = left.max(right);
    CHOPSTICKS[smaller].wait(); // 拿起编号小的筷子
    println!("Philosopher {} got left chopstick.", id);
    CHOPSTICKS[larger].wait(); // 拿起编号大的筷子
    println!("Philosopher {} got right chopstick.", id);
    delay_random();
    CHOPSTICKS[larger].signal(); // 放下编号大的筷子
    CHOPSTICKS[smaller].signal(); // 放下编号小的筷子
}

fn eat(id: usize, left: usize, right: usize) {
    println!("Philosopher {} want to eat.", id);
    CHOPSTICKS[left].wait(); // 拿起左边的筷子
    println!("Philosopher {} got left chopstick.", id);
    CHOPSTICKS[right].wait(); // 拿起右边的筷子
    println!("Philosopher {} got right chopstick.", id);
    delay_random();
    CHOPSTICKS[right].signal(); // 放下右边的筷子
    CHOPSTICKS[left].signal(); // 放下左边的筷子
}

#[inline(never)]
#[no_mangle]
fn delay() {
    for _ in 0..0x1000 {
        core::hint::spin_loop();
    }
}

#[inline(never)]
#[no_mangle]
fn delay_random() {
    let time = lib::sys_time();
    let pid = lib::sys_get_pid();
    let mut rng = ChaCha20Rng::seed_from_u64(time as u64 * pid as u64);
    let delay_time = rng.gen::<u64>() % 0xD00;
    for _ in 0..delay_time {
        core::hint::spin_loop();
    }
}

entry!(main);