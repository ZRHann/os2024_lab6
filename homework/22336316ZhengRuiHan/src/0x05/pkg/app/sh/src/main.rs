#![no_std]
#![no_main]
use core::arch::x86_64;
use core::str::from_utf8;
use lib::*;
extern crate lib;
fn main() -> isize {
    println!("YSOS 22336316 shell started! Type 'help' for help.");
    loop {
        print!("[>] ");
        let line = lib::stdin().read_line();
        let input = line.trim();
        match input {
            "help" => print_help(),
            "listapp" => sys_list_app(),
            "stat" => sys_stat(),
            "clear" => clear_screen(),
            cmd if cmd.starts_with("run ") => {
                let app = &cmd[4..];
                run_app(app);
            },
            _ => println!("Unknown command: {}", input),
        } 
    }
}

fn print_help() {
    println!("Shell Help:");
    println!("listapp - List all user programs");
    println!("stat - List all running processes");
    println!("run <program> - Run a user program");
    println!("clear - Clear the screen");
    println!("help - Display this help message");
    println!("YSOS 22336316");
}

fn clear_screen() {
    print!("\x1B[2J\x1B[1;1H");
}

fn run_app(app: &str) {
    let pid = sys_spawn(app);
    // if pid == 0 {
    //     println!("Failed to run app: {}", app); // BUG HERE
    // }
    sys_wait_pid(pid);
}

entry!(main);
