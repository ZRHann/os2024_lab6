#![no_std]
#![no_main]

use lib::*;
use core::arch::asm;
extern crate lib;
const THREAD_COUNT: usize = 16;
const QUEUE_CAP: usize = 8;
const MSG_COUNT: usize = 10;
static mut MESSAGE_QUEUE: MessageQueue = MessageQueue::new();


fn main() -> isize {
    unsafe {
        MESSAGE_QUEUE.init();
    }
    let mut pids = [0u16; THREAD_COUNT];
    for i in 0..THREAD_COUNT {
        let pid = sys_fork();
        if pid == 0 {
            if i % 2 == 0 {
                producer();
            } else {
                consumer();
            }
            sys_exit(0);
        } else {
            pids[i] = pid as u16;
        }
    }
    let cpid = sys_get_pid();
    sys_stat();
    println!("process #{} holds threads: {:?}", cpid, &pids);
    for i in 0..THREAD_COUNT {
        println!("#{} waiting for #{}...", cpid, pids[i]);
        sys_wait_pid(pids[i]);
    }
    sys_stat();
    println!("Final MESSAGE_QUEUE count: {}", unsafe { MESSAGE_QUEUE.count() });
    0
}

fn producer() {
    for i in 0..MSG_COUNT {
        let pid = sys_get_pid();
        unsafe {
            MESSAGE_QUEUE.push(i);
            // println!("Producer #{} produced message {}", pid, i);
        }
    }
}

fn consumer() {
    for _ in 0..MSG_COUNT {
        let pid = sys_get_pid();
        unsafe {
            let msg = MESSAGE_QUEUE.pop();
            // println!("Consumer #{} consumed message {}", pid, msg);
        }
        
    }
}

struct MessageQueue {
    queue: [usize; QUEUE_CAP],
    front: usize,
    rear: usize,
    count: usize,
    rwlock: SpinLock,
    not_empty: Semaphore,
    not_full: Semaphore,
}

impl MessageQueue {
    const fn new() -> Self {
        MessageQueue {
            queue: [0; QUEUE_CAP],
            front: 0,
            rear: 0,
            count: 0,
            rwlock: SpinLock::new(),
            not_empty: Semaphore::new(101), 
            not_full: Semaphore::new(102),
        }
    }

    fn init(&mut self) {
        self.not_empty.init(0); // 队列初始为空
        self.not_full.init(QUEUE_CAP); // 队列初始为满
    }

    fn push(&mut self, msg: usize) {
        let pid = sys_get_pid();
        if self.is_full() {
            println!("Producer {} blocked, queue is full", pid);
        }
        // sys_stat();
        self.not_full.wait(); // 阻塞直到队列不满
        {
            let guard = self.rwlock.acquire();
            if self.count < QUEUE_CAP {
                self.queue[self.rear] = msg;
                self.rear = (self.rear + 1) % QUEUE_CAP;
                self.count += 1;
                println!("Producer {} pushed message {}, new count is {}", pid, msg, self.count);
            }
        }
        self.not_empty.signal(); // 唤醒等待队列不空的消费者
    }

    fn pop(&mut self) -> usize {
        let pid = sys_get_pid();
        if self.is_empty() {
            println!("Consumer {} blocked, queue is empty", pid);
        }
        self.not_empty.wait(); // 阻塞直到队列不空
        let msg = {
            let guard = self.rwlock.acquire();
            let msg = if self.count > 0 {
                let msg = self.queue[self.front];
                self.front = (self.front + 1) % QUEUE_CAP;
                self.count -= 1;
                println!("Consumer {} popped message {}, new count is {}", pid, msg, self.count);
                msg
            } else {
                0 // 或者其他适当的默认值
            };
            msg
        };
        
        self.not_full.signal(); // 唤醒等待队列不满的生产者
        msg
    }

    fn is_full(&self) -> bool {
        self.count == QUEUE_CAP
    }

    fn is_empty(&self) -> bool {
        self.count == 0
    }

    fn count(&self) -> usize {
        self.count
    }
}
entry!(main);
