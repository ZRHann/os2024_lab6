#![no_std]
#![no_main]
use lib::*;
use lib::sleep::sleep;
extern crate lib;

fn main() -> isize {
    let time1 = sys_time();
    let sleep_time = 3;
    sleep(sleep_time);
    let time2 = sys_time();
    println!("Time elapsed: {} seconds", time2 - time1);
    0
}

entry!(main);