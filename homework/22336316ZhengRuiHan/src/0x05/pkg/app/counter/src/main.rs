#![no_std]
#![no_main]

use lib::*;
use core::arch::asm;
extern crate lib;
const THREAD_COUNT: usize = 32;
static mut COUNTER: isize = 0;
static SPIN_LOCK: SpinLock = SpinLock::new();
static SEMAPHORE: Semaphore = Semaphore::new(2829);

fn main() -> isize {
    test_semaphore();
    test_spin();
    0
}

fn test_spin() {
    let mut pids = [0u16; THREAD_COUNT];
    println!("test_spin: started");
    for i in 0..THREAD_COUNT {
        let pid = sys_fork();
        if pid == 0 { // child process
            do_counter_inc_spin();
            // println!("I am child process #{}", sys_get_pid());
            sys_exit(0);
        } else { // parent process
            pids[i] = pid as u16; // only parent knows child's pid
        }
    }
    let cpid = sys_get_pid();
    // sys_stat();
    println!("process #{} holds threads: {:?}", cpid, &pids);
    for i in 0..THREAD_COUNT {
        println!("#{} waiting for #{}...", cpid, pids[i]);
        sys_wait_pid(pids[i]);
    }
    println!("SPINLOCK COUNTER result: {}", unsafe { COUNTER });
}

fn test_semaphore() {    
    let mut pids = [0u16; THREAD_COUNT];
    SEMAPHORE.init(1);
    println!("test_semaphore: started");
    for i in 0..THREAD_COUNT {
        let pid = sys_fork();
        if pid == 0 {
            do_counter_inc_semaphore();
            sys_exit(0);
        } else {
            pids[i] = pid as u16; 
        }
    }

    let cpid = sys_get_pid();
    println!("process #{} holds threads: {:?}", cpid, &pids);
    // sys_stat();

    for i in 0..THREAD_COUNT {
        println!("#{} waiting for #{}...", cpid, pids[i]);
        sys_wait_pid(pids[i]);
    }
    println!("SEMAPHORE COUNTER result: {}", unsafe { COUNTER });
}

fn do_counter_inc_spin() {
    for _ in 0..100 {
        // Protect the critical section with SpinLock
        let guard = SPIN_LOCK.acquire();
        inc_counter();
    }
}

fn do_counter_inc_semaphore() {
    for _ in 0..100 {
        // Protect the critical section with Semaphore
        SEMAPHORE.wait();
        inc_counter();
        SEMAPHORE.signal();
    }
}



/// Increment the counter
///
/// this function simulate a critical section by delay
/// DO NOT MODIFY THIS FUNCTION
fn inc_counter() {
    unsafe {
        delay();
        let mut val = COUNTER;
        delay();
        val += 1;
        delay();
        COUNTER = val;
    }
}

#[inline(never)]
#[no_mangle]
fn delay() {
    for _ in 0..0x100 {
        core::hint::spin_loop();
    }
}

entry!(main);
