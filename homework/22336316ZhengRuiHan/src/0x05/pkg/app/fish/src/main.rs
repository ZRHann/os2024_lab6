#![no_std]
#![no_main]

use lib::*;
use core::{arch::asm, hint::spin_loop};
use rand::prelude::*;
use rand_chacha::ChaCha20Rng;
extern crate lib;

static RWLOCK :SpinLock = SpinLock::new();
static LEFT_ONLY :SpinLock = SpinLock::new();
static RIGHT_ONLY :SpinLock = SpinLock::new();
static UNDERSCORE_ONLY :SpinLock = SpinLock::new();
static mut IDX :usize = 0;
static mut last :char = ' ';
const LENGTH: usize = 1027;

fn main() -> isize {
    let mut pids = [0u16; 3];
    
    for i in 0..3 {
        let pid = sys_fork();
        if pid == 0 {
            match i {
                0 => process_left(),
                1 => process_right(),
                2 => process_underscore(),
                _ => (),
            }
            sys_exit(0);
        } else {
            pids[i] = pid as u16;
        }
    }
    
    for &pid in &pids {
        // println!("waiting for pid: {}", pid);
        sys_wait_pid(pid);
    }
    println!("");

    0
}

fn process_left() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            let guard = RWLOCK.acquire();
            if IDX % 4 == 3 || last == '<' {
                continue;
            }
            print!("<");
            last = '<';
            IDX += 1;
        }
    }
}

fn process_right() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            let guard = RWLOCK.acquire();
            if IDX % 4 == 3 || last == '>' {
                continue;
            }
            print!(">");
            last = '>';
            IDX += 1;
        }
    }
}

fn process_underscore() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            let guard = RWLOCK.acquire();
            if IDX % 4 != 3 {
                continue;
            }
            print!("_");
            last = '_';
            IDX += 1;
        }
    }
}

#[inline(never)]
#[no_mangle]
fn delay() {
    for _ in 0..0x10000 {
        core::hint::spin_loop();
    }
}

#[inline(never)]
#[no_mangle]
fn delay_random() {
    let time = lib::sys_time();
    let pid = lib::sys_get_pid();
    let mut rng = ChaCha20Rng::seed_from_u64(time as u64 * pid as u64);
    let delay_time = rng.gen::<u64>() % 0x1000;
    for _ in 0..delay_time {
        core::hint::spin_loop();
    }
}

entry!(main);
