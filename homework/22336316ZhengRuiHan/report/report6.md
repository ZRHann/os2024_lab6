**操作系统原理 实验六（RUST）**

## 个人信息

【院系】计算机学院

【专业】计算机科学与技术

【学号】22336316

【姓名】郑锐涵

## 实验题目

操作系统的启动

## 实验目的

1. 了解 fork 的实现原理，实现 fork 系统调用。
2. 了解并发与锁机制的概念，实现基于操作系统的自旋锁、信号量。
3. 编写基于 fork 的并发程序，并测试自旋锁、信号量的正确性。

## 实验要求

- [fork 的实现](https://ysos.gzti.me/labs/0x05/tasks/#fork-的实现)
- [系统调用](https://ysos.gzti.me/labs/0x05/tasks/#系统调用)
- [进程管理](https://ysos.gzti.me/labs/0x05/tasks/#进程管理)
- [功能测试](https://ysos.gzti.me/labs/0x05/tasks/#功能测试)
- [并发与锁机制](https://ysos.gzti.me/labs/0x05/tasks/#并发与锁机制)
- [原子指令](https://ysos.gzti.me/labs/0x05/tasks/#原子指令)
- [自旋锁](https://ysos.gzti.me/labs/0x05/tasks/#自旋锁)
- [信号量](https://ysos.gzti.me/labs/0x05/tasks/#信号量)
- [测试任务](https://ysos.gzti.me/labs/0x05/tasks/#测试任务)
- [多线程计数器](https://ysos.gzti.me/labs/0x05/tasks/#多线程计数器)
- [消息队列](https://ysos.gzti.me/labs/0x05/tasks/#消息队列)
- [哲学家的晚饭](https://ysos.gzti.me/labs/0x05/tasks/#哲学家的晚饭)
- [思考题](https://ysos.gzti.me/labs/0x05/tasks/#思考题)
- [加分项](https://ysos.gzti.me/labs/0x05/tasks/#加分项)

## 实验方案

RUST路线。

## 实验过程

### 更新代码框架

实验文档和框架进行了一部分的更新，具体体现在原本属于 `ProcessData` 的栈数据现在全部被迁移到了 `ProcessVm` 中。所以先相应地更新代码。

```rust
    fn grow_stack(
        &mut self,
        addr: VirtAddr,
        mapper: MapperRef,
        alloc: FrameAllocatorRef,
    ) -> Result<(), MapToError<Size4KiB>> {
        debug_assert!(self.is_on_stack(addr), "Address is not on stack.");

        // grow stack for page fault
        let start_page = Page::<Size4KiB>::containing_address(addr);
        let start_addr = start_page.start_address().as_u64();
        let page_count = self.range.start - start_page;
        elf::map_range(
            start_addr, 
            page_count, 
            mapper, 
            alloc, 
            true, // TODO: 对内核栈来说，这里应该是false
            false
        ).expect("grow_stack: Stack mapping failed");
        self.range.start = start_page;
        Ok(())
    }
```

其它接口做对应的修改。

### fork的实现

```rust
// service.rs
pub fn fork_process(args: &SyscallArgs, context: &mut ProcessContext) {
    // fork current process, return pid
    proc::fork(context);
}
```

```rust
// proc/mod.rs
pub fn fork(context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        // save_current as parent
        manager.save_current(context);
        // fork to get child
        let child_pid = manager.fork();
        // push to child & parent to ready queue
        if manager.current().write().status() == ProgramStatus::Ready {
            manager.push_ready(manager.current().pid());
        }
        if manager.get_proc(&child_pid).unwrap().write().status() == ProgramStatus::Ready {
            manager.push_ready(child_pid);
        }
        // switch to next process
        manager.switch_next(context);
    })
}
```

```rust
	// manager.rs
	pub fn fork(&self) -> ProcessId {
        // get current process
        let parent = self.current();
        // fork to get child
        let child = parent.fork();
        let child_pid = child.pid();
        // add child to process list
        self.add_proc(child_pid, child);
        // print the process ready queue?
        child_pid
    }
```

```rust
	// process.rs Process
	pub fn fork(self: &Arc<Self>) -> Arc<Self> {
        // lock inner as write
        // FIXME: inner fork with parent weak ref
        let parent_weak: Weak<Process> = Arc::downgrade(self);
        let child_inner = {
            let self_inner = self.inner.read();
            self_inner.fork(parent_weak)
        };
        let child_pid = child_inner.pid;

        // FOR DBG: maybe print the child process info
        //          e.g. parent, name, pid, etc.
        debug!("Forking process: parent_pid={}, name={}", self.pid(), self.inner.read().name());

        // make the arc of child
        let child_process = Arc::new(Process {
            pid: child_pid,
            inner: Arc::new(RwLock::new(child_inner)),
        });
        // add child to current process's children list
        self.inner.write().children.push(child_process.clone()); // Arc的clone, 增加引用计数，而不是深拷贝
        // set fork ret value for parent with `context.set_rax`
        self.inner.write().context.set_rax(child_pid.0.into());
        // mark the child as ready & return it
        child_process
    }
```

```rust
    pub fn fork(&self, parent: Weak<Process>) -> ProcessInner {
        // fork the process virtual memory struct
        let parent_arc = parent.upgrade().unwrap();
        let parent_inner = parent_arc.read();
        // calculate the real stack offset
        let child_count = parent_inner.children.len() as u64 + 1;
        let chlid_vm: Option<ProcessVm> = Some(parent_inner.vm().fork(child_count * STACK_MAX_SIZE));
        // update rsp, rbp
        let mut child_context = parent_inner.context.clone();
        child_context.stack_frame.stack_pointer -= child_count * STACK_MAX_SIZE;
        // child_context.regs.rbp -= child_count as usize * STACK_MAX_SIZE as usize;
        // set the return value 0 for child with `context.set_rax`
        child_context.set_rax(0);
        // clone the process data struct
        let child_data = Some(parent_inner.proc_data().clone());
        // construct the child process inner
        let child_inner = ProcessInner {
            pid: ProcessId::new(),
            name: parent_inner.name().to_string(),
            parent: Some(parent.clone()),
            children: Vec::new(),
            ticks_passed: 0,
            status: ProgramStatus::Ready,
            exit_code: None,
            context: child_context,
            proc_vm: chlid_vm,
            proc_data: child_data,
        };

        // NOTE: return inner because there's no pid record in inner
        child_inner
    }
```

### fork功能测试

对文档给的测试函数做了一些修复：

```rust
#![no_std]
#![no_main]
extern crate alloc;
extern crate lib;
use lib::*;
static mut M: u64 = 0xdeadbeef;
fn main() -> isize {
    let mut c = 32;
    let pid = sys_fork();
    if pid == 0 {
        println!("I am the child process");
        assert_eq!(c, 32);
        unsafe {
            println!("child read value of M: {:#x}", M);
            M = 0x2333;
            println!("child changed the value of M: {:#x}", M);
        }
        c += 32;
    } else {
        println!("I am the parent process");
        sys_stat();
        assert_eq!(c, 32);
        println!("assert_eq!(c, 32) PASSED");
        println!("Waiting for child to exit...");
        while sys_wait_pid(pid as u16) < 0 {}
        let ret = sys_wait_pid(pid as u16);
        println!("Child exited with status {}", ret);
        assert_eq!(ret, 64);
        unsafe {
            println!("parent read value of M: {:#x}", M);
            assert_eq!(M, 0x2333);
        }
        c += 1024;
        assert_eq!(c, 1056);
    }
    c
}
entry!(main);
```

测试结果：

![image-20240523034309894](report6.assets/image-20240523034309894.png)

符合预期。

### 进程的阻塞与唤醒

```rust
// proc.rs
pub fn wait_pid(pid: ProcessId, context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        if let Some(ret) = manager.get_exit_code(pid) {
            context.set_rax(ret.try_into().unwrap()); // return the exit code of the process if exited
        } else {
            context.set_rax(usize::MAX - 2); // return -2 if process is not exited yet. return in WAITER's context
            manager.wait_pid(pid);
            manager.save_current(context);
            manager.current().write().block();
            // debug!("context1: {:?}", context);
            manager.switch_next(context);
            // manager.print_process_list();
            // debug!("context2: {:?}", context);
            // let proc = manager.get_proc(&pid).unwrap();
            // debug!("process #{:?}: \nstatus: {:?}", pid, proc.read().status());
        }
    })
}
```

```rust
	// manager.rs
	/// Block the process with the given pid
    pub fn block(&self, pid: ProcessId) {
        if let Some(proc) = self.get_proc(&pid) {
            proc.write().block();
        }
    }

    pub fn wait_pid(&self, pid: ProcessId) {
        let mut wait_queue = self.wait_queue.lock();
        // push the current process to the wait queue
        //   `processor::current_pid()` is waiting for `pid`
        let current_pid: ProcessId = processor::current_pid();
        if !wait_queue.contains_key(&pid) {
            wait_queue.insert(pid, BTreeSet::new());
        }
        wait_queue.get_mut(&pid).unwrap().insert(current_pid);
    }

    /// Wake up the process with the given pid
    ///
    /// If `ret` is `Some`, set the return value of the process
    pub fn wake_up(&self, pid: ProcessId, ret: Option<isize>) {
        if let Some(proc) = self.get_proc(&pid) {
            // let mut inner = proc.write();
            if let Some(ret) = ret {
                // set the return value of the process
                //        like `context.set_rax(ret as usize)`
                proc.write().set_rax(ret as usize);
            }
            // set the process as ready
            proc.write().resume();
            // push to ready queue
            self.push_ready(pid);
        }
    }
```

然后修改wait pid系统调用的实现：

```rust
pub fn sys_wait_pid(args: &SyscallArgs, context: &mut ProcessContext) {
    // block current process, wait for process with pid
    // return the exit code of the process if exited
    // return -1 if process does not exist
    // return -2 if process is not exited yet
    let pid = args.arg0 as u16;
    let manager = get_process_manager();
    match manager.get_proc(&ProcessId(pid)) {
        Some(proc) => {
            proc::wait_pid(ProcessId(pid), context);
        },
        None => {
            context.set_rax(usize::MAX - 1); // -1
        }
    }
}
```

再修改shell，去掉忙等待逻辑：

```rust
fn run_app(app: &str) {
    let pid = sys_spawn(app);
    if pid == 0 {
        println!("Failed to run app: {}", app);
    }
    sys_wait_pid(pid);
}
```

![image-20240524214835599](report6.assets/image-20240524214835599.png)

符合预期。

同时，更新testfork并重新测试：

```rust
#![no_std]
#![no_main]
extern crate alloc;
extern crate lib;
use lib::*;
static mut M: u64 = 0xdeadbeef;
fn main() -> isize {
    let mut c = 32;
    let pid = sys_fork();
    if pid == 0 {
        println!("I am the child process");
        assert_eq!(c, 32);
        unsafe {
            println!("child read value of M: {:#x}", M);
            M = 0x2333;
            println!("child changed the value of M: {:#x}", M);
        }
        c += 32;
    } else {
        println!("I am the parent process");
        sys_stat();
        assert_eq!(c, 32);
        println!("assert_eq!(c, 32) PASSED");
        println!("Waiting for child to exit...");
        sys_wait_pid(pid as u16);
        let ret = sys_wait_pid(pid as u16);
        assert_eq!(ret, 64);
        unsafe {
            println!("parent read value of M: {:#x}", M);
            assert_eq!(M, 0x2333);
        }
        c += 1024;
        assert_eq!(c, 1056);
    }
    c
}
entry!(main);
```

一开始测出非法访存，调试发现是因为fork里没有更新子进程的rbp。

修改后符合预期。

![image-20240525034002159](report6.assets/image-20240525034002159.png)

### 自旋锁的实现

```rust
impl SpinLock {
    pub const fn new() -> Self {
        Self {
            bolt: AtomicBool::new(false), // true means locked
        }
    }

    pub fn acquire(&self) {
        // acquire the lock, spin if the lock is not available
        while self.bolt.compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed).is_err() {
            spin_loop(); // 提高性能，编译成pause指令。
        }
    }

    pub fn release(&self) {
        // release the lock
        self.bolt.store(false, Ordering::Release);
    }
}
```

### 信号量的实现

由于标准linux并没有定义sys_sem，所以定义Sem系统调用号为：

```
Sem = 65530
```

```rust
// service.rs
pub fn sys_sem(args: &SyscallArgs, context: &mut ProcessContext) {
    match args.arg0 {
        0 => context.set_rax(new_sem(args.arg1 as u32, args.arg2)),
        1 => context.set_rax(remove_sem(args.arg1 as u32)),
        2 => sem_signal(args.arg1 as u32, context),
        3 => sem_wait(args.arg1 as u32, context),
        _ => context.set_rax(usize::MAX),
    }
}

pub fn sem_wait(key: u32, context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        let pid = current_pid();
        let ret = manager.current().write().sem_wait(key, pid);
        match ret {
            SemaphoreResult::Ok => context.set_rax(0),
            SemaphoreResult::NotExist => context.set_rax(1),
            SemaphoreResult::Block(pid) => {
                // FIXME: save, block it, then switch to next
                manager.save_current(context);
                manager.current().write().block();
                // manager.block_current();
                manager.switch_next(context);
            }
            _ => unreachable!(),
        }
    })
}

pub fn sem_signal(key: u32, context: &mut ProcessContext) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let manager = get_process_manager();
        let ret = manager.current().write().sem_signal(key);
        match ret {
            SemaphoreResult::Ok => context.set_rax(0),
            SemaphoreResult::NotExist => context.set_rax(1),
            SemaphoreResult::WakeUp(pid) => {
                // wake up the process
                manager.get_proc(&pid).unwrap().write().resume();
                manager.push_ready(pid);
                context.set_rax(0);
            }
            _ => unreachable!(),
        }
    })
}

pub fn new_sem(key: u32, value: usize) -> usize {
    let manager = get_process_manager();
    if manager.current().write().sem_insert(key, value) {
        0
    } else {
        1
    }
}

pub fn remove_sem(key: u32) -> usize {
    let manager = get_process_manager();
    if manager.current().write().sem_remove(key) {
        0
    } else {
        1
    }
}
```

```rust
	// process.rs
	pub fn sem_wait(&self, key: u32, pid: ProcessId) -> SemaphoreResult{
        self.proc_data().semaphores.write().wait(key, pid)
    }

    pub fn sem_signal(&self, key: u32) -> SemaphoreResult {
        self.proc_data().semaphores.write().signal(key)
    }

    pub fn sem_insert(&self, key: u32, value: usize) -> bool {
        self.proc_data().semaphores.write().insert(key, value)
    }

    pub fn sem_remove(&self, key: u32) -> bool {
        self.proc_data().semaphores.write().remove(key)
    }
```

```rust
// sync.rs
impl Semaphore {
    /// Create a new semaphore
    pub fn new(value: usize) -> Self {
        Self {
            count: value,
            wait_queue: VecDeque::new(),
        }
    }

    /// Wait the semaphore (acquire/down/proberen)
    ///
    /// if the count is 0, then push the process into the wait queue. 
    /// else decrease the count and return Ok
    pub fn wait(&mut self, pid: ProcessId) -> SemaphoreResult {
        // if the count is 0, then push pid into the wait queue
        //     return Block(pid)
        if self.count == 0 {
            self.wait_queue.push_back(pid);
            SemaphoreResult::Block(pid)
        } else { 
            // else decrease the count and return Ok
            self.count -= 1;
            SemaphoreResult::Ok
        }
    }

    /// Signal the semaphore (release/up/verhogen)
    ///
    /// if the wait queue is not empty, then pop a process from the wait queue
    /// else increase the count
    pub fn signal(&mut self) -> SemaphoreResult {
        // if the wait queue is not empty
        //          pop a process from the wait queue
        //          return WakeUp(pid)
        // FIXME: else increase the count and return Ok
        if let Some(pid) = self.wait_queue.pop_front() {
            SemaphoreResult::WakeUp(pid)
        } else {
            self.count += 1;
            SemaphoreResult::Ok
        }
    }
}

#[derive(Debug, Default)]
pub struct SemaphoreSet {
    sems: BTreeMap<SemaphoreId, Mutex<Semaphore>>,
}

impl SemaphoreSet {
    pub fn insert(&mut self, key: u32, value: usize) -> bool { // value is count
        trace!("Sem Insert: <{:#x}>{}", key, value);
        // insert a new semaphore into the sems
        //          use `insert(/* ... */).is_none()`
        self.sems.insert(SemaphoreId::new(key), Mutex::new(Semaphore::new(value))).is_none()
    }

    pub fn remove(&mut self, key: u32) -> bool {
        trace!("Sem Remove: <{:#x}>", key);
        // FIXME: remove the semaphore from the sems
        //          use `remove(/* ... */).is_some()`
        self.sems.remove(&SemaphoreId::new(key)).is_some()
    }

    /// Wait the semaphore (acquire/down/proberen)
    pub fn wait(&self, key: u32, pid: ProcessId) -> SemaphoreResult {
        let sid = SemaphoreId::new(key);
        // try get the semaphore from the sems then do it's operation
        if let Some(sem) = self.sems.get(&sid) {
            let mut sem = sem.lock();
            sem.wait(pid)
        } else {
            // return NotExist if the semaphore is not exist
            SemaphoreResult::NotExist
        }
    }

    /// Signal the semaphore (release/up/verhogen)
    pub fn signal(&self, key: u32) -> SemaphoreResult {
        let sid = SemaphoreId::new(key);
        // try get the semaphore from the sems then do it's operation
        let sid = SemaphoreId::new(key);
        if let Some(sem) = self.sems.get(&sid) {
            let mut sem = sem.lock();
            sem.signal()
        } else { 
            // return NotExist if the semaphore is not exist
            SemaphoreResult::NotExist
        }
    }
}
```

```rust
// lib/sync.rs

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Semaphore {
    key: u32, 
}

impl Semaphore {
    pub const fn new(key: u32) -> Self {
        Semaphore { key }
    }

    #[inline(always)]
    pub fn init(&self, value: usize) {
        sys_new_sem(self.key, value);
    }

    #[inline(always)]
    pub fn wait(&self) {
        sys_sem_wait(self.key);
    }

    #[inline(always)]
    pub fn signal(&self) {
        sys_sem_signal(self.key);
    }

    #[inline(always)]
    pub fn remove(&self) {
        sys_remove_sem(self.key);
    }
}
unsafe impl Sync for Semaphore {}
```

```rust
// lib/syscall.rs
#[inline(always)]
pub fn sys_fork() -> usize {
    syscall!(Syscall::Fork) as usize
}

#[inline(always)]
pub fn sys_new_sem(key: u32, value: usize) -> usize {
    syscall!(Syscall::Sem, 0, key as u64, value as u64) as usize
}

#[inline(always)]
pub fn sys_remove_sem(key: u32) -> usize {
    syscall!(Syscall::Sem, 1, key as u64) as usize
}

#[inline(always)]
pub fn sys_sem_signal(key: u32) -> usize {
    syscall!(Syscall::Sem, 2, key as u64) as usize
}

#[inline(always)]
pub fn sys_sem_wait(key: u32) -> usize {
    syscall!(Syscall::Sem, 3, key as u64) as usize
}
```

### 多线程计数器

```rust
#![no_std]
#![no_main]

use lib::*;
use core::arch::asm;
extern crate lib;
const THREAD_COUNT: usize = 32;
static mut COUNTER: isize = 0;
static SPIN_LOCK: SpinLock = SpinLock::new();
static SEMAPHORE: Semaphore = Semaphore::new(2829);

fn main() -> isize {
    test_semaphore();
    test_spin();
    0
}

fn test_spin() {
    let mut pids = [0u16; THREAD_COUNT];
    println!("test_spin: started");
    for i in 0..THREAD_COUNT {
        let pid = sys_fork();
        if pid == 0 { // child process
            do_counter_inc_spin();
            // println!("I am child process #{}", sys_get_pid());
            sys_exit(0);
        } else { // parent process
            pids[i] = pid as u16; // only parent knows child's pid
        }
    }
    let cpid = sys_get_pid();
    // sys_stat();
    println!("process #{} holds threads: {:?}", cpid, &pids);
    for i in 0..THREAD_COUNT {
        println!("#{} waiting for #{}...", cpid, pids[i]);
        sys_wait_pid(pids[i]);
    }
    println!("SPINLOCK COUNTER result: {}", unsafe { COUNTER });
}

fn test_semaphore() {    
    let mut pids = [0u16; THREAD_COUNT];
    SEMAPHORE.init(1);
    println!("test_semaphore: started");
    for i in 0..THREAD_COUNT {
        let pid = sys_fork();
        if pid == 0 {
            do_counter_inc_semaphore();
            sys_exit(0);
        } else {
            pids[i] = pid as u16; 
        }
    }

    let cpid = sys_get_pid();
    println!("process #{} holds threads: {:?}", cpid, &pids);
    // sys_stat();

    for i in 0..THREAD_COUNT {
        println!("#{} waiting for #{}...", cpid, pids[i]);
        sys_wait_pid(pids[i]);
    }
    println!("SEMAPHORE COUNTER result: {}", unsafe { COUNTER });
}

fn do_counter_inc_spin() {
    for _ in 0..100 {
        // Protect the critical section with SpinLock
        SPIN_LOCK.acquire();
        inc_counter();
        SPIN_LOCK.release();
    }
}

fn do_counter_inc_semaphore() {
    for _ in 0..100 {
        // Protect the critical section with Semaphore
        SEMAPHORE.wait();
        inc_counter();
        SEMAPHORE.signal();
    }
}



/// Increment the counter
///
/// this function simulate a critical section by delay
/// DO NOT MODIFY THIS FUNCTION
fn inc_counter() {
    unsafe {
        delay();
        let mut val = COUNTER;
        delay();
        val += 1;
        delay();
        COUNTER = val;
    }
}

#[inline(never)]
#[no_mangle]
fn delay() {
    for _ in 0..0x100 {
        core::hint::spin_loop();
    }
}

entry!(main);

```

对于自旋锁和信号量，分别创建32个子进程。

![image-20240527103422387](report6.assets/image-20240527103422387.png)

结果符合预期。

### 消息队列

```rust
#![no_std]
#![no_main]

use lib::*;
use core::arch::asm;
extern crate lib;
const THREAD_COUNT: usize = 16;
const QUEUE_CAP: usize = 8;
const MSG_COUNT: usize = 10;
static mut MESSAGE_QUEUE: MessageQueue = MessageQueue::new();


fn main() -> isize {
    unsafe {
        MESSAGE_QUEUE.init();
    }
    let mut pids = [0u16; THREAD_COUNT];
    for i in 0..THREAD_COUNT {
        let pid = sys_fork();
        if pid == 0 {
            if i % 2 == 0 {
                producer();
            } else {
                consumer();
            }
            sys_exit(0);
        } else {
            pids[i] = pid as u16;
        }
    }
    let cpid = sys_get_pid();
    sys_stat();
    println!("process #{} holds threads: {:?}", cpid, &pids);
    for i in 0..THREAD_COUNT {
        println!("#{} waiting for #{}...", cpid, pids[i]);
        sys_wait_pid(pids[i]);
    }
    sys_stat();
    println!("Final MESSAGE_QUEUE count: {}", unsafe { MESSAGE_QUEUE.count() });
    0
}

fn producer() {
    for i in 0..MSG_COUNT {
        let pid = sys_get_pid();
        unsafe {
            MESSAGE_QUEUE.push(i);
            // println!("Producer #{} produced message {}", pid, i);
        }
    }
}

fn consumer() {
    for _ in 0..MSG_COUNT {
        let pid = sys_get_pid();
        unsafe {
            let msg = MESSAGE_QUEUE.pop();
            // println!("Consumer #{} consumed message {}", pid, msg);
        }
        
    }
}

struct MessageQueue {
    queue: [usize; QUEUE_CAP],
    front: usize,
    rear: usize,
    count: usize,
    rwlock: SpinLock,
    not_empty: Semaphore,
    not_full: Semaphore,
}

impl MessageQueue {
    const fn new() -> Self {
        MessageQueue {
            queue: [0; QUEUE_CAP],
            front: 0,
            rear: 0,
            count: 0,
            rwlock: SpinLock::new(),
            not_empty: Semaphore::new(101), 
            not_full: Semaphore::new(102),
        }
    }

    fn init(&mut self) {
        self.not_empty.init(0); // 队列初始为空
        self.not_full.init(QUEUE_CAP); // 队列初始为满
    }

    fn push(&mut self, msg: usize) {
        let pid = sys_get_pid();
        if self.is_full() {
            println!("Producer {} blocked, queue is full", pid);
        }
        // sys_stat();
        self.not_full.wait(); // 阻塞直到队列不满
        self.rwlock.acquire();
        if self.count < QUEUE_CAP {
            self.queue[self.rear] = msg;
            self.rear = (self.rear + 1) % QUEUE_CAP;
            self.count += 1;
            println!("Producer {} pushed message {}, new count is {}", pid, msg, self.count);
        }
        self.rwlock.release();
        self.not_empty.signal(); // 唤醒等待队列不空的消费者
    }

    fn pop(&mut self) -> usize {
        let pid = sys_get_pid();
        if self.is_empty() {
            println!("Consumer {} blocked, queue is empty", pid);
        }
        self.not_empty.wait(); // 阻塞直到队列不空
        self.rwlock.acquire();
        let msg = if self.count > 0 {
            let msg = self.queue[self.front];
            self.front = (self.front + 1) % QUEUE_CAP;
            self.count -= 1;
            println!("Consumer {} popped message {}, new count is {}", pid, msg, self.count);
            msg
        } else {
            0 // 或者其他适当的默认值
        };
        self.rwlock.release();
        self.not_full.signal(); // 唤醒等待队列不满的生产者
        msg
    }

    fn is_full(&self) -> bool {
        self.count == QUEUE_CAP
    }

    fn is_empty(&self) -> bool {
        self.count == 0
    }

    fn count(&self) -> usize {
        self.count
    }
}
entry!(main);
```

在队列为空时，消费者被阻塞。在队列满时，生产者被阻塞。

队列容量为1：

![image-20240527103805828](report6.assets/image-20240527103805828.png)

队列容量为4：

![image-20240527103840268](report6.assets/image-20240527103840268.png)

队列容量为8：

![image-20240527103918239](report6.assets/image-20240527103918239.png)

队列容量为16：

![image-20240527104000998](report6.assets/image-20240527104000998.png)

### 哲学家的晚饭

```rust
#![no_std]
#![no_main]

extern crate lib;
use lib::*;
use core::arch::asm;
use rand::prelude::*;
use rand_chacha::ChaCha20Rng;
const PHILOSOPHER_COUNT: usize = 5;
static CHOPSTICKS: [Semaphore; PHILOSOPHER_COUNT] = semaphore_array![0, 1, 2, 3, 4];

fn main() -> isize {
    for i in 0..PHILOSOPHER_COUNT {
        CHOPSTICKS[i].init(1);
    }
    let mut pids = [0u16; PHILOSOPHER_COUNT];
    for i in 0..PHILOSOPHER_COUNT {
        let pid = sys_fork();
        if pid == 0 {
            philosopher(i);
            sys_exit(0);
        } else {
            pids[i] = pid as u16;
        }
    }

    for i in 0..PHILOSOPHER_COUNT {
        sys_wait_pid(pids[i]);
    }

    0
}

fn philosopher(id: usize) {
    let left = id;
    let right = (id + 1) % PHILOSOPHER_COUNT;
    
    loop {
        think(id);
        eat(id, left, right);
        // better_eat(id, left, right);
    }
}

fn think(id: usize) {
    println!("Philosopher {} is thinking.", id);
    delay_random();
    // delay();
}

fn better_eat(id: usize, left: usize, right: usize) {
    println!("Philosopher {} want to eat.", id);
    let smaller = left.min(right);
    let larger = left.max(right);
    CHOPSTICKS[smaller].wait(); // 拿起编号小的筷子
    println!("Philosopher {} got left chopstick.", id);
    CHOPSTICKS[larger].wait(); // 拿起编号大的筷子
    println!("Philosopher {} got right chopstick.", id);
    delay_random();
    CHOPSTICKS[larger].signal(); // 放下编号大的筷子
    CHOPSTICKS[smaller].signal(); // 放下编号小的筷子
}

fn eat(id: usize, left: usize, right: usize) {
    println!("Philosopher {} want to eat.", id);
    CHOPSTICKS[left].wait(); // 拿起左边的筷子
    println!("Philosopher {} got left chopstick.", id);
    CHOPSTICKS[right].wait(); // 拿起右边的筷子
    println!("Philosopher {} got right chopstick.", id);
    delay_random();
    CHOPSTICKS[right].signal(); // 放下右边的筷子
    CHOPSTICKS[left].signal(); // 放下左边的筷子
}

#[inline(never)]
#[no_mangle]
fn delay() {
    for _ in 0..0x1000 {
        core::hint::spin_loop();
    }
}

#[inline(never)]
#[no_mangle]
fn delay_random() {
    let time = lib::sys_time();
    let pid = lib::sys_get_pid();
    let mut rng = ChaCha20Rng::seed_from_u64(time as u64 * pid as u64);
    let delay_time = rng.gen::<u64>() % 0xD00;
    for _ in 0..delay_time {
        core::hint::spin_loop();
    }
}

entry!(main);
```

其中，eat为没有解决死锁的随机方法。better_eat为使用[资源分级解法](https://zh.wikipedia.org/wiki/%E5%93%B2%E5%AD%A6%E5%AE%B6%E5%B0%B1%E9%A4%90%E9%97%AE%E9%A2%98#%E8%B5%84%E6%BA%90%E5%88%86%E7%BA%A7%E8%A7%A3%E6%B3%95)的无死锁方法。

随机方法：

![image-20240527104332561](report6.assets/image-20240527104332561.png)

可以观察到：

1. 某些哲学家能够成功就餐，即同时拿到左右两侧的筷子：

   ![image-20240527104418017](report6.assets/image-20240527104418017.png)

   2. 尝试构造死锁情况，即所有哲学家都无法同时拿到他们需要的筷子：

      最后程序卡住，为死锁情况。

   3. 尝试构造饥饿情况，即某些哲学家无法获得足够的机会就餐：

      ![image-20240527104545053](report6.assets/image-20240527104545053.png)

      可以观察到1号哲学家经过较长时间才得到右筷子，为饥饿情况。

      对于解决方法，使用了[资源分级解法](https://zh.wikipedia.org/wiki/%E5%93%B2%E5%AD%A6%E5%AE%B6%E5%B0%B1%E9%A4%90%E9%97%AE%E9%A2%98#%E8%B5%84%E6%BA%90%E5%88%86%E7%BA%A7%E8%A7%A3%E6%B3%95)解决了死锁问题。也即完成了**加分项2**.

      > 另一个简单的解法是为资源（这里是餐叉）分配一个[偏序](https://zh.wikipedia.org/wiki/偏序)或者分级的关系，并约定所有资源都按照这种顺序获取，按相反顺序释放，而且保证不会有两个无关资源同时被同一项工作所需要。在哲学家就餐问题中，资源（餐叉）按照某种规则编号为1至5，每一个工作单元（哲学家）总是先拿起左右两边编号较低的餐叉，再拿编号较高的。用完餐叉后，他总是先放下编号较高的餐叉，再放下编号较低的。在这种情况下，当四位哲学家同时拿起他们手边编号较低的餐叉时 ，只有编号最高的餐叉留在桌上，从而第五位哲学家就不能使用任何一只餐叉了。而且，只有一位哲学家能使用最高编号的餐叉，所以他能使用两只餐叉用餐。当他吃完后，他会先放下编号最高的餐叉，再放下编号较低的餐叉，从而让另一位哲学家拿起后边的这只开始吃东西。

      ![image-20240527104818294](report6.assets/image-20240527104818294.png)

程序一直运行，没有发生死锁，这验证了正确性。

### 思考题

**在 Lab 2 中设计输入缓冲区时，如果不使用无锁队列实现，而选择使用 `Mutex` 对一个同步队列进行保护，在编写相关函数时需要注意什么问题？考虑在进行 `pop` 操作过程中遇到串口输入中断的情形，尝试描述遇到问题的场景，并提出解决方案。**

`Mutex`（互斥锁）使用阻塞的方式实现锁的获取。如果一个线程尝试获取已经被其他线程持有的 `Mutex`，该线程会被挂起（阻塞），直到 `Mutex` 被释放。

pop时，如果队列为空则锁被长期占有，如果此时遇到串口输入中断，其无法获取锁来push，队列也就长期为空，导致死锁。

改用自旋锁可以解决。



**在进行 `fork` 的复制内存的过程中，系统的当前页表、进程页表、子进程页表、内核页表等之间的关系是怎样的？在进行内存复制时，需要注意哪些问题？**

当前页表 == 父进程的页表。

子进程的页表与父进程的页表共享。

它们都在创建父进程时，从内核页表克隆。

在进行内存复制时，只需复制栈内存。复制完成后，需要修改子进程rsp。



**为什么在实验的实现中，`fork` 系统调用必须在任何 Rust 内存分配（堆内存分配）之前进行？如果在堆内存分配之后进行 `fork`，会有什么问题？**

因为父子进程共享了堆内存空间（页表）。

如果堆内存分配后fork，由于父子进程得到的堆内存地址是一样的，可能存在线程安全问题。例如，父进程修改堆内存会导致子进程的堆内存也被修改。又如，子进程释放堆内存而父进程访问之，会导致错误。



**进行原子操作时候的 `Ordering` 参数是什么？此处 Rust 声明的内容与 [C++20 规范](https://en.cppreference.com/w/cpp/atomic/memory_order) 中的一致，尝试搜索并简单了解相关内容，简单介绍该枚举的每个值对应于什么含义。**

1. **`Relaxed`**：不对其他读或写操作进行排序，只保证操作本身是原子的。
2. **`Acquire`**：保证当前操作之后的所有读写操作不会被重排序到之前，适用于从其他线程获取数据的同步场景。
3. **`Release`**：保证当前操作之前的所有读写操作不会被重排序到之后，适用于将数据发布给其他线程的场景。
4. **`AcqRel`**（AcquireRelease）：同时具有 `Acquire` 和 `Release` 的效果，适用于读-改-写操作。
5. **`SeqCst`**（Sequentially Consistent）：提供最强的内存排序保证，所有的原子操作按顺序执行，适用于需要最严格同步的场景。

对于`AcqRel`和`SeqCst`有如下区别：

- `AcqRel`：保证一个线程的操作对其他线程可见，确保特定线程操作前后的顺序，但不保证所有线程看到的顺序一致。
- `SeqCst`：保证所有线程看到的操作顺序一致，提供最严格的同步保证。



**在实现 `SpinLock` 的时候，为什么需要实现 `Sync` trait？类似的 `Send` trait 又是什么含义？**

`Sync` trait的含义是：如果一个类型`T`是`Sync`，那么引用`&T`可以安全地在线程间共享。如果不实现`Sync` trait，编译器就不会认为SpinLock`是线程安全的，无法在多个线程之间共享引用。

`Send` trait的含义是：如果一个类型`T`是`Send`，那么`T`可以安全地在线程间转移所有权。

然而，去掉

```
unsafe impl Sync for Semaphore {}
```

这一行，仍然能通过编译。猜测是为了后续实现。



**`core::hint::spin_loop` 使用的 `pause` 指令和 Lab 4 中的 `x86_64::instructions::hlt` 指令有什么区别？这里为什么不能使用 `hlt` 指令？**

`pause` 指令主要用于忙等待循环（busy-wait loop）中的优化，提示处理器当前正在执行一个忙等待循环，从而优化电源管理和性能。

`hlt`指令停止处理器的执行，直到硬件中断发生。

`hlt`在自旋锁中是不合适的，因为等待锁的线程或处理器需要频繁检查锁的状态。如通过硬件中断来唤醒，会导致响应时间较慢。

### 加分项

**🤔 尝试实现如下用户程序任务，完成用户程序** `fish`

- 创建三个子进程，让它们分别能输出且只能输出 `>`，`<` 和 `_`。
- 使用学到的方法对这些子进程进行同步，使得打印出的序列总是 `<><_` 和 `><>_` 的组合。

在完成这一任务的基础上，其他细节可以自行决定如何实现，包括输出长度等。

```rust
#![no_std]
#![no_main]

use lib::*;
use core::{arch::asm, hint::spin_loop};
use rand::prelude::*;
use rand_chacha::ChaCha20Rng;
extern crate lib;

static RWLOCK :SpinLock = SpinLock::new();
static LEFT_ONLY :SpinLock = SpinLock::new();
static RIGHT_ONLY :SpinLock = SpinLock::new();
static UNDERSCORE_ONLY :SpinLock = SpinLock::new();
static mut IDX :usize = 0;
static mut last :char = ' ';
const LENGTH: usize = 1027;

fn main() -> isize {
    let mut pids = [0u16; 3];
    
    for i in 0..3 {
        let pid = sys_fork();
        if pid == 0 {
            match i {
                0 => process_left(),
                1 => process_right(),
                2 => process_underscore(),
                _ => (),
            }
            sys_exit(0);
        } else {
            pids[i] = pid as u16;
        }
    }
    
    for &pid in &pids {
        // println!("waiting for pid: {}", pid);
        sys_wait_pid(pid);
    }
    println!("");

    0
}

fn process_left() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            RWLOCK.acquire();
                if IDX % 4 == 3 || last == '<' {
                    RWLOCK.release();
                    continue;
                }
                print!("<");
                last = '<';
                IDX += 1;
            RWLOCK.release();
        }
    }
}

fn process_right() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            RWLOCK.acquire();
                if IDX % 4 == 3 || last == '>' {
                    RWLOCK.release();
                    continue;
                }
                print!(">");
                last = '>';
                IDX += 1;
            RWLOCK.release();
        }
    }
}

fn process_underscore() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            RWLOCK.acquire();
                if IDX % 4 != 3 {
                    RWLOCK.release();
                    continue;
                }
                print!("_");
                last = '_';
                IDX += 1;
            RWLOCK.release();
        }
    }
}

#[inline(never)]
#[no_mangle]
fn delay() {
    for _ in 0..0x10000 {
        core::hint::spin_loop();
    }
}

#[inline(never)]
#[no_mangle]
fn delay_random() {
    let time = lib::sys_time();
    let pid = lib::sys_get_pid();
    let mut rng = ChaCha20Rng::seed_from_u64(time as u64 * pid as u64);
    let delay_time = rng.gen::<u64>() % 0x1000;
    for _ in 0..delay_time {
        core::hint::spin_loop();
    }
}

entry!(main);
```

思路是使用锁让同一时刻只有一个进程在工作，然后使用条件判断来控制输出符合要求。

![image-20240527105308276](report6.assets/image-20240527105308276.png)

**🤔 尝试和前文不同的其他方法解决哲学家就餐问题，并验证你的方法能够正确解决它，简要介绍你的方法，并给出程序代码和测试结果。**

使用了[资源分级解法](https://zh.wikipedia.org/wiki/%E5%93%B2%E5%AD%A6%E5%AE%B6%E5%B0%B1%E9%A4%90%E9%97%AE%E9%A2%98#%E8%B5%84%E6%BA%90%E5%88%86%E7%BA%A7%E8%A7%A3%E6%B3%95)解决。见**哲学家的晚饭**部分。



**🔥 尝试使用符合 Rust 做法的方式处理互斥锁，使用 RAII 的方式来保证锁的释放：**

`Deref` trait 不仅仅用于解引用操作（使用 `*` 符号），还可以用于：

- **自动解引用**：当调用一个方法时，Rust 会自动解引用，直到找到方法为止。也可以可以通过自动解引用来访问内部字段。

- **自动索引操作**：如果解引用到的类型可以索引（`[]`），那么被解引用的类型也可以自动索引到解引用的类型。

- **自动解引用强制转换**：Rust 会自动将 `&T` 转换为 `&U`，只要 `T: Deref<Target = U>`。这在函数参数传递时非常有用。

生命周期参数（如 `'a`）主要是在编译时起作用，帮助Rust编译器检查引用的有效性，确保引用不会变成悬挂引用，从而避免潜在的内存安全问题。它们不会在运行时产生影响。

首先定义`SpinLockGuard`：

```rust
pub struct SpinLockGuard<'a> {
    lock: &'a SpinLock,
}

// 离开作用域时，会自动调用drop方法解锁。
impl<'a> Drop for SpinLockGuard<'a> {
    fn drop(&mut self) {
        // self.lock.release();
        self.bolt.store(false, Ordering::Release);
    }
}

impl<'a> core::ops::Deref for SpinLockGuard<'a> {
    type Target = SpinLock;

    fn deref(&self) -> &Self::Target {
        &self.lock
    }
}
```

 修改`acquire`为返回一个`SpinLockGuard`：

```rust
    pub fn acquire(&self) -> SpinLockGuard {
        // acquire the lock, spin if the lock is not available
        while self.bolt.compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed).is_err() {
            spin_loop(); // 提高性能，编译成pause指令。
        }
        SpinLockGuard { lock: self }
    }
```

注释掉release函数。

然后修改用户程序：

```rust
// counter
fn do_counter_inc_spin() {
    for _ in 0..100 {
        // Protect the critical section with SpinLock
        let guard = SPIN_LOCK.acquire();
        inc_counter();
    }
}
```

```rust
// fish
fn process_left() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            let guard = RWLOCK.acquire();
            if IDX % 4 == 3 || last == '<' {
                continue;
            }
            print!("<");
            last = '<';
            IDX += 1;
        }
    }
}

fn process_right() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            let guard = RWLOCK.acquire();
            if IDX % 4 == 3 || last == '>' {
                continue;
            }
            print!(">");
            last = '>';
            IDX += 1;
        }
    }
}

fn process_underscore() {
    unsafe {
        while IDX < LENGTH {
            delay_random();
            let guard = RWLOCK.acquire();
            if IDX % 4 != 3 {
                continue;
            }
            print!("_");
            last = '_';
            IDX += 1;
        }
    }
}
```

```rust
    fn push(&mut self, msg: usize) {
        let pid = sys_get_pid();
        if self.is_full() {
            println!("Producer {} blocked, queue is full", pid);
        }
        // sys_stat();
        self.not_full.wait(); // 阻塞直到队列不满
        {
            let guard = self.rwlock.acquire();
            if self.count < QUEUE_CAP {
                self.queue[self.rear] = msg;
                self.rear = (self.rear + 1) % QUEUE_CAP;
                self.count += 1;
                println!("Producer {} pushed message {}, new count is {}", pid, msg, self.count);
            }
        }
        self.not_empty.signal(); // 唤醒等待队列不空的消费者
    }

    fn pop(&mut self) -> usize {
        let pid = sys_get_pid();
        if self.is_empty() {
            println!("Consumer {} blocked, queue is empty", pid);
        }
        self.not_empty.wait(); // 阻塞直到队列不空
        let msg = {
            let guard = self.rwlock.acquire();
            let msg = if self.count > 0 {
                let msg = self.queue[self.front];
                self.front = (self.front + 1) % QUEUE_CAP;
                self.count -= 1;
                println!("Consumer {} popped message {}, new count is {}", pid, msg, self.count);
                msg
            } else {
                0 // 或者其他适当的默认值
            };
            msg
        };
        
        self.not_full.signal(); // 唤醒等待队列不满的生产者
        msg
    }
```

需要注意`let guard =` 是必须的。因为需要有一个变量guard来承载生命周期于作用域，否则返回的`SpinLockGuard`的生命周期将只局限于 `rwlock.acquire()`一行代码。

修改后，测试结果与原来相同，符合预期。

## 实验总结

### 自旋锁（SpinLock）

**优点**：

1. **低延迟**：自旋锁在锁等待期间不会发生上下文切换，适合在临界区短的情况下使用，延迟较低。
2. **简单实现**：实现简单，适用于无阻塞、短时间的临界区保护。

**缺点**：

1. **高CPU占用**：在锁等待期间会一直占用CPU资源，不适合长时间的临界区保护。
2. **不适合多核环境下的高并发**：在高并发的情况下，自旋锁可能导致大量的CPU时间浪费在忙等待上。

### 信号量（Semaphore）

**优点**：

1. **低CPU占用**：在等待期间，线程会被阻塞，释放CPU资源，适合长时间的临界区保护。
2. **适用广泛**：可以用于各种资源同步，包括读写锁、计数器等。

**缺点**：

1. **高延迟**：由于涉及上下文切换，信号量在锁等待期间会有一定的延迟。
2. **实现复杂**：实现相对复杂，特别是在处理多个条件和资源时。

## 参考文献

[哲学家就餐问题-维基百科](https://zh.wikipedia.org/wiki/%E5%93%B2%E5%AD%A6%E5%AE%B6%E5%B0%B1%E9%A4%90%E9%97%AE%E9%A2%98)

https://en.cppreference.com/w/cpp/atomic/memory_order
